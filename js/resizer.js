var soundIsOn = true;
$(document).ready(function() {
    //loadMenu();
    $(window).resize(function() {
        setParameters();
    });
    setParameters();
});

var getBodySize = function() {
    var w = $('body').width();
    var h = $('body').height();
    return {w: w, h: h};
};

function estimateParameters(bodySize) {
    var newW = 0;
    var newH = 0;
    if (bodySize.w > bodySize.h * 2) {
        newW = bodySize.h * 2;
        newH = bodySize.h;
    } else if (bodySize.h > bodySize.w / 2) {
        newH = bodySize.w / 2;
        newW = bodySize.w;
    } else {
        var area1 = 0;
        var area2 = 0;

        area1 = bodySize.w * bodySize.w / 2;
        area2 = bodySize.h * 2 * bodySize.h;

        if (area1 > area2) {
            newW = bodySize.w;
            newH = bodySize.w / 2;
        } else {
            newW = bodySize.h * 2;
            newH = bodySize.h;
        }
    }

    var percentageW = Math.round(((bodySize.w - newW) / 2 ) * 100 / bodySize.w);
    var percentageH = Math.round(((bodySize.h - newH) / 2 ) * 100 / bodySize.h);
    return {newW: newW, newH: newH, percentageW: percentageW, percentageH: percentageH};
};
function setParametersToMain(parameters) {
    var newW = parameters.newW;
    var newH = parameters.newH;
    var percentageW = parameters.percentageW;
    var percentageH = parameters.percentageH;

    $("#main").css({
        left: percentageW + '%',
        top: percentageH + '%',
        width: newW + 'px',
        height: newH + 'px',
        maxWidth: newW + 'px',
        maxHeight: newH + 'px'
    });
};

var setParametersToMenu = function(min) {
    $("#menu").css("height", min + "px");
    $("#menu").css("min-height", min + "px");
    $("#menu").css("max-height", min + "px");

    $("#menu").css("width", min + "px");
    $("#menu").css("min-width", min + "px");
    $("#menu").css("max-width", min + "px");
};

var setMenuActivity = function() {
    console.log("menu activity setting");
    var start = $("#start");
    var question = $("#question");
    var sound = $("#sound");
    var levelButtons = [];
    for (var  i = 0; i < 8; ++i) {
        levelButtons[i] = $("#level_button_" + (i + 1).toString());
    }
    start.unbind();
    question.unbind();
    sound.unbind();

    start.bind(getOnTouchStartEventName(), menuStartTouchStart);
    question.bind(getOnTouchStartEventName(), menuQuestionTouchStart);
    sound.bind(getOnTouchStartEventName(), menuSoundTouchStart);

    start.bind(getOnTouchEndEventName(), menuStartTouchEnd);
    question.bind(getOnTouchEndEventName(), menuQuestionTouchEnd);
    sound.bind(getOnTouchEndEventName(), menuSoundTouchEnd);

    for (var  i = 0; i < 8; ++i) {
        levelButtons[i].unbind();
        levelButtons[i].bind(getOnTouchStartEventName(), levelButtonTouchStart);
        levelButtons[i].bind(getOnTouchStartEventName(), levelButtonTouchEnd);
    }
};

var hideNotExistingLevelsInMenu = function() {
    var levels = angular.element($('body')).scope().levels;
    //console.log(angular.element($('body')).scope().game);
    console.log("hide non existing menu levels: " + levels);

    for (var i = 1; i <= 8; ++i) {
        if (i > levels) {
            $("#level_button_" + i).hide();
        }
    }
};

var setParameters = function() {
    console.log("Set parameters called");
    var bodySize = getBodySize();
//    console.log(bodySize);
    var parameters = estimateParameters(bodySize);
    setParametersToMain(parameters);

    if (window.textFontResizer != null && window.textFontResizer != undefined) {
        textFontResizer(w);
    } else {
        console.log("Warning: textFontResizer is not defined or null, font sizes are not adaptive/responsive");
    }

    var w = $("#main").width();
//    var fontSize = w * 0.0650;
//    $("#main p").css('font-size', fontSize);

    $("#points_text_l3").css('font-size', w * 0.06);
    $("#gametextp").css('font-size', w * 0.025);
};


var menuStartTouchStart = function(e) {
    var game = angular.element($('body')).scope();

    if (game.startDisabled) {
        return;
    }

    var start = $("#start");

    start.removeClass("start_green");
    start.removeClass("start_red");
    start.addClass("start_pressed");
};

var menuQuestionTouchStart = function(e) {
    var game = angular.element($('body')).scope();

    if (game.questionButtonDisabled) {
        return;
    }

    var question = $("#question");

    question.removeClass("question_green");
    question.removeClass("question_red");
    question.addClass("question_pressed");
};

var menuSoundTouchStart = function(e) {
    var sound = $("#sound");

    if (soundIsOn) {
        sound.removeClass("sound_green");
        sound.addClass("sound_pressed");
    } else {
        sound.removeClass("sound_muted");
        sound.addClass("sound_muted_pressed");
    }

};

var menuSoundTouchEnd = function(e) {
    var game = angular.element($('body')).scope();
    var sound = $("#sound");
    if (game.soundIsOn) {
        sound.addClass("sound_muted");
        sound.removeClass("sound_green");
        sound.removeClass("sound_pressed");
    } else {
        sound.removeClass("sound_muted");
        sound.removeClass("sound_pressed");
        sound.removeClass("sound_muted_pressed");
        sound.addClass("sound_green");
    }

    game.soundIsOn = !game.soundIsOn;
    game.toggleSound();
};

var menuStartTouchEnd = function(e) {
    var game = angular.element($('body')).scope();
    if (game.startDisabled) {
        return;
    }
    var start = $("#start");

    start.addClass("start_green");
    start.removeClass("start_red");
    start.removeClass("start_pressed");

    game.startPress();
};

var menuQuestionTouchEnd = function(e) {
    console.log("menu question touch end");
    var game = angular.element($('body')).scope();
    if (game.questionButtonDisabled) {
        return;
    }

    var question = $("#question");

    question.addClass("question_green");
    question.removeClass("question_red");
    question.removeClass("question_pressed");

    game.questionPress();
};

var levelButtonTouchStart = function(e) {
    var element = $(e.target);
    var idNum = element.attr("id").slice(-1);

    console.log(idNum);

    var elem = $("#level_button_" + (idNum).toString());

    elem.removeClass("level_button_green_" + idNum.toString());
    elem.removeClass("level_button_red_" + idNum.toString());
    elem.addClass("level_button_pressed_" + idNum.toString());
};

var levelButtonTouchEnd = function(e) {
    var game = angular.element($('body')).scope();
    var element = $(e.target);
    var idNum = element.attr("id").slice(-1);

    var elem = $("#level_button_" + (idNum).toString());
    elem.addClass("level_button_green_" + idNum.toString());
    elem.removeClass("level_button_red_" + idNum.toString());
    elem.removeClass("level_button_pressed_" + idNum.toString());

    game.levelSelected(parseInt(idNum));
    $("#gametextp").css('font-size', fontSize/3);
};