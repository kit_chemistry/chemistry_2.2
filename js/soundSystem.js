function SoundSystem() {
    this.tracks = {};
    this.isMuted = false;
    this.toggleMute = function() {
        this.isMuted = !this.isMuted;

        for(var key in this.tracks) {

            var track = this.tracks[key];
            for (var i = 0; i < track.sounds.length; ++i) {
                track.sounds[i].muted = this.isMuted;
            }

        }
    };

    this.loadSound = function(name, path, instances) {
        var sound = new Audio(path);
        sound.load();

        var track = {
            numberOfTracks: instances,
            sounds: [],
            currentTrackNumber: 0
        };

        for (var i = 0; i < instances; ++i) {
            var clonedNode = sound.cloneNode();
            track.sounds.push(sound.cloneNode());
        }

        $("audio").each(function(ind, elem) {
            console.log(elem);
            elem.muted =  true;
        });

        this.tracks[name] = track;
        return sound;
    }

    this.loadCommon = function() {
        this.loadSound('right', 'sound/right.mp3', 5);
        this.loadSound('wrong', 'sound/wrong.mp3', 5);
    };

    this.play = function(name, ended) {
        var track = this.tracks[name];
        if (track == undefined) {
            console.log("No sound for " + name);
            if (ended != null) {
                ended();
            }
            return;
        }
        var num = track.numberOfTracks;
        var cur = track.currentTrackNumber;
        track.sounds[cur].removeEventListener("ended", ended);

        if (ended != undefined && ended != null) {
            track.sounds[cur].addEventListener("ended", ended);
        }

        track.sounds[cur].play();

        if (cur + 1 == num) {
            cur = 0;
        } else {
            ++cur;
        }
        track.currentTrackNumber = cur;
    }

    this.rem = function(name,ended){
        //console.log();
        var track = this.tracks[name];
        var num = track.numberOfTracks;
        var cur = track.currentTrackNumber;
        var app = track.sounds[cur];
        console.log(app);

    }

    this.stop = function(name) {
        var track = this.tracks[name];
        var num = track.numberOfTracks;
        var cur = track.currentTrackNumber;

        cur--;
        if (cur < 0) {
            cur = num - 1;
        }

        track.sounds[cur].pause();
        track.sounds[cur].currentTime = 0;
    }
}