
var isTouchSupported = "ontouchend" in document;

var getOnTouchEndEventName = function() {
    if (isTouchSupported) {
        return "touchend";
    } else {
        return "mouseup";
    }
};

var getOnTouchStartEventName = function() {
    if (isTouchSupported) {
        return "touchstart";
    } else {
        return "mousedown";
    }
};

var resizeNisEmblem = function(width) {
    var w1 = ($("body").innerWidth() - width) / 2;
    var h1 = $("body").innerHeight();
    var leftPart = 0;

    w1 = Math.round(w1 < h1 ? w1 : h1);
    var wpercent = Math.round(15600 / w1);
    var hpercent = Math.round(9600 / w1);

    $("#upperBlock").css("max-height", w1 + "px");
    $("#upperBlock").css("max-width", w1 + "px");
    $("#upperBlock").css("min-height", w1 + "px");
    $("#upperBlock").css("min-width", w1 + "px");
    $("#upperBlock").css("height", w1 + "px");
    $("#upperBlock").css("width", w1 + "px");
    //$("#upperBlock").css("left", Math.floor(leftPart + width) + "px");
};

var isAToTheRightOfB = function(objA, objB) {
    var posAX = objA.offset().left;
    var posAY = objA.offset().top;

    var widthA = objA.width();
    var heightA = objA.height();

    var posBX = objB.offset().left;
    var posBY = objB.offset().top;

    var widthB = objB.width();
    var heightB = objB.height();

    console.log("Ax > Bx -> " + posAX + " " + posBX);
    return (posAX > posBX);
};

var doIntersectWithPoint = function(point, objB) {
    var posAX = point.left;
    var posAY = point.top;

    var posBX = objB.offset().left;
    var posBY = objB.offset().top;

    var widthB = objB.width();
    var heightB = objB.height();

//    console.log("posA X, Y: " + posAX + ", " + posAY);
//    console.log("posB X, Y: " + posBX + ", " + posBY);
//    console.log("W x H: " + widthB + " x " + heightB);

    if (posAY <= posBY + heightB && posAY >= posBY
        && posAX <= posBX + widthB && posAX >= posBX) {
        return true;
    }
    return false;
};

var doIntersect = function(objA, objB) {
    var posAX = objA.offset().left;
    var posAY = objA.offset().top;

    var widthA = objA.width();
    var heightA = objA.height();

    var posBX = objB.offset().left;
    var posBY = objB.offset().top;

    var widthB = objB.width();
    var heightB = objB.height();
    ////console.log(objB);
//
//    console.log("do intersect function:");
//    console.log("(posAX, posAY) = (" + posAX + ", " + posAY + ")");
//    console.log("(posBX, posBY) = (" + posBX + ", " + posBY + ")");
//    console.log("(widthA, heightA) = (" + widthA + ", " + heightA + ")");
//    console.log("(widthB, heightB) = (" + widthB + ", " + heightB + ")");

    if (posAY <= posBY && posBY <= posAY + heightA &&
        posAX <= posBX && posBX <= posAX + widthA) {
        return true;
    } else if (posBY <= posAY && posAY <= posBY + heightB &&
        posAX <= posBX && posBX <= posAX + widthA) {
        return true;
    } else if (posBY <= posAY && posAY <= posBY + heightB &&
        posBX <= posAX && posAX <= posBX + widthB) {
        return true;
    } else if (posAY <= posBY && posBY <= posAY + heightA &&
        posBX <= posAX && posAX <= posBX + widthB) {
        return true;
    }
    return false;
};

var getAreaOfIntersection = function(objA, objB) {
    var posAX = objA.offset().left;
    var posAY = objA.offset().top;

    var widthA = objA.width();
    var heightA = objA.height();

    var posBX = objB.offset().left;
    var posBY = objB.offset().top;

    var widthB = objB.width();
    var heightB = objB.height();
    ////console.log(objB);
//
//    console.log("do intersect function:");
//    console.log("(posAX, posAY) = (" + posAX + ", " + posAY + ")");
//    console.log("(posBX, posBY) = (" + posBX + ", " + posBY + ")");
//    console.log("(widthA, heightA) = (" + widthA + ", " + heightA + ")");
//    console.log("(widthB, heightB) = (" + widthB + ", " + heightB + ")");

    var w = 0, h = 0;
    if (posAY <= posBY && posBY <= posAY + heightA &&
        posAX <= posBX && posBX <= posAX + widthA) {
        if (posBX + widthB >= posAX + widthA) {
            w = posAX + widthA - posBX;
        } else {
            w = widthB;
        }

        if (posBY + heightB >= posAY + heightA) {
            h = posAY + heightA - posBY;
        } else {
            h = heightB;
        }

        return w * h;
    } else if (posBY <= posAY && posAY <= posBY + heightB &&
        posAX <= posBX && posBX <= posAX + widthA) {

        if (posAY + heightA <= posBY + heightB) {
            h = heightB;
        } else {
            h = posBY + heightB - posAY;
        }

        if (posBX + widthB <= posAX + widthA) {
            w = widthB;
        } else {
            w = widthA + posAX - posBX;
        }
        return w * h;
    } else if (posBY <= posAY && posAY <= posBY + heightB &&
        posBX <= posAX && posAX <= posBX + widthB) {

        if (posBY + heightB <= posAY + heightA) {
            h = heightB - posAY;
        } else {
            h = heightA;
        }

        if (posBX + widthB <= posAX + widthA) {
            w = widthB - posAX;
        } else {
            w = widthA;
        }

        return w * h;

    } else if (posAY <= posBY && posBY <= posAY + heightA &&
        posBX <= posAX && posAX <= posBX + widthB) {

        if (posBY + heightB <= posAY + heightA) {
            h = heightB - posAY;
        } else {
            h = heightA;
        }

        if (posAX + widthA <= posBX + widthB) {
            w = widthA - posBX;
        } else {
            w = widthB;
        }

        return w * h;
    }
    return 0;
};

function shuffle(array) {
    var currentIndex = array.length, temporaryValue, randomIndex ;

    // While there remain elements to shuffle...
    while (0 !== currentIndex) {

        // Pick a remaining element...
        randomIndex = Math.floor(Math.random() * currentIndex);
        currentIndex -= 1;

        // And swap it with the current element.
        temporaryValue = array[currentIndex];
        array[currentIndex] = array[randomIndex];
        array[randomIndex] = temporaryValue;
    }

    return array;
}

function signForTriangle(p1, p2, p3)
{
    return (p1.x - p3.x)
        * (p2.y - p3.y)
        -  (p2.x - p3.x)
        * (p1.y - p3.y);
}

function isPointInTriangle(pt, v1, v2, v3)
{
    var b1, b2, b3;

    b1 = signForTriangle(pt, v1, v2) < 0.0;
    b2 = signForTriangle(pt, v2, v3) < 0.0;
    b3 = signForTriangle(pt, v3, v1) < 0.0;

    return ((b1 == b2) && (b2 == b3));
};

function isPointInRectangle(pt, v1, v2, v3, v4) {

    if (v1.x <= pt.x && pt.x <= v2.x && v1.y <= pt.y && pt.y <= v3.y) {
        return true;
    }
    return false;
};

function moveObjects(from, to, objects, deltaTime, stepSize, waitTimeBeforeFuncCall, func) {
    var leftShift = (to.x - from.x) / stepSize;
    var topShift = (to.y - from.y) / stepSize;
    var counter = 0;

    var move = function() {
        for (var a = 0; a < objects.length; ++a) {
            var obj = objects[a];
            var offset = obj.offset();
            var newtop = offset.top + topShift;
            var newleft = offset.left + leftShift;
            obj.offset({top: newtop, left: newleft});
        }

        ++counter;

        if (counter === stepSize) {
            setTimeout(function () {
                func();
            }, waitTimeBeforeFuncCall);
        } else {
            setTimeout(move, deltaTime);
        }
    };
    setTimeout(move, deltaTime);
};

function calculateCenterOfMass(points) {
    var result = {x: 0, y: 0};

    for (var i = 0; i < points.length; ++i) {
        result.x += points[i].x;
        result.y += points[i].y;
    }

    result.x /= points.length;
    result.y /= points.length;
    return result;
};

function throwException(reason, object) {
    throw reason || "Assertion failed";
};

function sortArray(arr, asc) {
    var check = false;
    var len = arr.length;
    var result = [];
    for (var i = 0; i < len - 1; ++i) {
        for (var j = i + 1; j < len; ++j) {
            if (asc) {
                check = arr[i] > arr[j];
            } else {
                check = arr[i] < arr[j];
            }

            if (check) {
                var temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
            }
        }
    }
    return arr;
};

//CANVAS RELATED
/*
 * Get mouse event coordinate converted to canvas coordinate
 * c: The canvas object
 * e: A mouse event
 */
function toCanvasX(c, e) {
    var posx = 0;

    if (e.pageX)   {
        posx = e.pageX;
    } else if (e.clientX)   {
        posx = e.clientX + document.body.scrollLeft
            + document.documentElement.scrollLeft;
    }
    posx = posx - c.offsetLeft;
    return posx;
}

function toCanvasY(c, e) {
    var posy = 0;

    if (e.pageY)   {
        posy = e.pageY;
    } else if (e.clientY)   {
        posy = e.clientY + document.body.scrollTop
            + document.documentElement.scrollTop;
    }
    posy = posy - c.offsetTop;

    return posy;
}


var getBodySize = function() {
    var w = $('body').width();
    var h = $('body').height();
    return {w: w, h: h};
};