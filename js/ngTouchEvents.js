var app;

var isTouchSupported = "ontouchend" in document;

var getOnTouchEndEventName = function () {
    if (isTouchSupported) {
        return "touchend";
    } else {
        return "mouseup";
    }
};

var getOnTouchStartEventName = function () {
    if (isTouchSupported) {
        return "touchstart";
    } else {
        return "mousedown";
    }
};

app.directive("ngPressstart", [function () {
    return function (scope, elem, attrs) {
        elem.bind(getOnTouchStartEventName(), function (e) {
            e.preventDefault();
            e.stopPropagation();

            scope.$apply(attrs["ngPressstart"]);
        });
    }
}]);

app.directive("ngPressend", [function () {
    return function (scope, elem, attrs) {
        elem.bind(getOnTouchEndEventName(), function (e) {
            e.preventDefault();
            e.stopPropagation();

            scope.$apply(attrs["ngPressend"]);
        });
    }
}]);
