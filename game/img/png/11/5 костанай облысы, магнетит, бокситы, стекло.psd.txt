#up_8{
	background: transparent url('../img/level/up_8.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 18.65%;
	top: 89.5%;
	width: 1.55%;
	height: 2.2%;
}

#up_7{
	background: transparent url('../img/level/up_7.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 10.85%;
	top: 89.5%;
	width: 1.55%;
	height: 2.2%;
}

#down_8{
	background: transparent url('../img/level/down_8.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 18.65%;
	top: 96.2%;
	width: 1.55%;
	height: 2.2%;
}

#down_7{
	background: transparent url('../img/level/down_7.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 10.85%;
	top: 96.2%;
	width: 1.55%;
	height: 2.2%;
}

#up_6{
	background: transparent url('../img/level/up_6.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 43.95%;
	top: 89.1%;
	width: 1.55%;
	height: 2.2%;
}

#up_5{
	background: transparent url('../img/level/up_5.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 36.15%;
	top: 89.1%;
	width: 1.55%;
	height: 2.2%;
}

#down_6{
	background: transparent url('../img/level/down_6.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 43.95%;
	top: 95.7%;
	width: 1.55%;
	height: 2.2%;
}

#down_5{
	background: transparent url('../img/level/down_5.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 36.15%;
	top: 95.7%;
	width: 1.55%;
	height: 2.2%;
}

#up_4{
	background: transparent url('../img/level/up_4.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 43.75%;
	top: 51.5%;
	width: 1.55%;
	height: 2.2%;
}

#up_3{
	background: transparent url('../img/level/up_3.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 35.95%;
	top: 51.5%;
	width: 1.55%;
	height: 2.2%;
}

#down_4{
	background: transparent url('../img/level/down_4.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 43.75%;
	top: 58.1%;
	width: 1.55%;
	height: 2.2%;
}

#down_3{
	background: transparent url('../img/level/down_3.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 35.95%;
	top: 58.1%;
	width: 1.55%;
	height: 2.2%;
}

#up_2{
	background: transparent url('../img/level/up_2.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 18.35%;
	top: 51.5%;
	width: 1.55%;
	height: 2.2%;
}

#up_1{
	background: transparent url('../img/level/up_1.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 10.55%;
	top: 51.5%;
	width: 1.55%;
	height: 2.2%;
}

#down_2{
	background: transparent url('../img/level/down_2.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 18.35%;
	top: 58.1%;
	width: 1.55%;
	height: 2.2%;
}

#down_1{
	background: transparent url('../img/level/down_1.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 10.55%;
	top: 58.1%;
	width: 1.55%;
	height: 2.2%;
}

#bauxites{
	background: transparent url('../img/level/bauxites.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 30.55%;
	top: 18.4%;
	width: 37.6%;
	height: 64.6%;
}

#O{
	background: transparent url('../img/level/O.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 49.4%;
	top: 51.8%;
	width: 4.15%;
	height: 16%;
}

#Al{
	background: transparent url('../img/level/Al.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 42.85%;
	top: 51.9%;
	width: 5.8%;
	height: 15.8%;
}

#formula_6/3=2_6/2=3{
	background: transparent url('../img/level/formula_6/3=2_6/2=3.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 52.3%;
	top: 71.8%;
	width: 13.55%;
	height: 4.90%;
}

#formula_3*2=6{
	background: transparent url('../img/level/formula_3*2=6.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 34.15%;
	top: 71.7%;
	width: 8.6%;
	height: 4.7%;
}

#pozdravlyaiu{
	background: transparent url('../img/level/pozdravlyaiu.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 19.05%;
	top: 54.5%;
	width: 54.2%;
	height: 27.3%;
}

#illustrtions{
	background: transparent url('../img/level/illustrtions.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 4.75%;
	top: 22.4%;
	width: 41.1%;
	height: 73.1%;
}

#tablica_valentnosti{
	background: transparent url('../img/level/tablica_valentnosti.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 50.2%;
	top: -1.8%;
	width: 48.75%;
	height: 98%;
}

#zadacha_zhelezo{
	background: transparent url('../img/level/zadacha_zhelezo.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 11.4%;
	top: 6.6%;
	width: 54%;
	height: 83.2%;
}

#formula{
	background: transparent url('../img/level/formula.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 2.95%;
	top: 1.6%;
	width: 46.35%;
	height: 20.4%;
}

#logo{
	background: transparent url('../img/level/logo.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 27.5%;
	top: -0.9%;
	width: 49.75%;
	height: 97.2%;
}

#bg{
	background: transparent url('../img/level/bg.png') 0 0 no-repeat;
	background-size: 100% 100%;
	position: absolute;
	left: 0%;
	top: 0%;
	width: 100%;
	height: 100%;
}

<div id='up_8' ng-class=''></div>
<div id='up_7' ng-class=''></div>
<div id='down_8' ng-class=''></div>
<div id='down_7' ng-class=''></div>
<div id='up_6' ng-class=''></div>
<div id='up_5' ng-class=''></div>
<div id='down_6' ng-class=''></div>
<div id='down_5' ng-class=''></div>
<div id='up_4' ng-class=''></div>
<div id='up_3' ng-class=''></div>
<div id='down_4' ng-class=''></div>
<div id='down_3' ng-class=''></div>
<div id='up_2' ng-class=''></div>
<div id='up_1' ng-class=''></div>
<div id='down_2' ng-class=''></div>
<div id='down_1' ng-class=''></div>
<div id='bauxites' ng-class=''></div>
<div id='O' ng-class=''></div>
<div id='Al' ng-class=''></div>
<div id='formula_6/3=2_6/2=3' ng-class=''></div>
<div id='formula_3*2=6' ng-class=''></div>
<div id='pozdravlyaiu' ng-class=''></div>
<div id='illustrtions' ng-class=''></div>
<div id='tablica_valentnosti' ng-class=''></div>
<div id='zadacha_zhelezo' ng-class=''></div>
<div id='formula' ng-class=''></div>
<div id='logo' ng-class=''></div>
<div id='bg' ng-class=''></div>
