var app = angular.module('game1', []);

var sound1 = new Audio('sounds/sound1.mp3'); sound1.load();
var sound2 = new Audio('sounds/sound2.mp3'); sound2.load();
var sound3 = new Audio('sounds/sound3.mp3'); sound3.load();
var sound4 = new Audio('sounds/sound4.mp3'); sound4.load();
var sound5 = new Audio('sounds/sound5.mp3'); sound5.load();
var sound6 = new Audio('sounds/sound6.mp3'); sound6.load();
var sound7 = new Audio('sounds/sound7.mp3'); sound7.load();
var sound8 = new Audio('sounds/sound8.mp3'); sound8.load();
var sound9 = new Audio('sounds/sound9.mp3'); sound9.load();

var sounds = [sound1, sound2, sound3, sound4, sound5, sound6, sound7, sound8, sound9];

var textFontResizer = function() {
    var w = $("#main").width();
    var fontSize = w * 0.02;
    var fontSize2 = w * 0.017;
    var fontSize3 = w * 0.02;

    var fontSize4 = w * 0.02;

    var fontSizeAkmObl = w * 0.02;
    var fontSizeKarObl = w * 0.024;
    var fontSizeTable7 = w * 0.022;

    var fontSizeTaskText = w * 0.02;

    var fontSizePoleHint = w * 0.02;

    var fontSizeTur1 = w * 0.02;
    var fontSizeChemCells = w * 0.025;

    var fontSizeAnswersS8 = w * 0.018;

    $("#hint_text").css('font-size', Math.round(fontSizeAnswersS8) + "px");

    $("#task_text1_s8").css('font-size', Math.round(fontSizeAnswersS8) + "px");
    $("#task_text2_s8").css('font-size', Math.round(fontSizeAnswersS8) + "px");
    $("#task_text3_s8").css('font-size', Math.round(fontSizeAnswersS8) + "px");

    $(".chem_text_cells").css('font-size', Math.round(fontSizeChemCells) + "px");
    $(".tur_inputs").css('font-size', Math.round(fontSizeTur1) + "px");
    $(".tur_letters").css('font-size', Math.round(fontSizeTur1) + "px");

    $("#hint_pole1").css('font-size', Math.round(fontSizeTaskText) + "px");
    $("#hint_pole2").css('font-size', Math.round(fontSizeTaskText) + "px");
    $("#hint_pole3").css('font-size', Math.round(fontSizeTaskText) + "px");
    $("#hint_pole4").css('font-size', Math.round(fontSizeTaskText) + "px");

    $("#task_pole").css('font-size', Math.round(fontSizeTaskText) + "px");

    $("#t7t1").css('font-size', Math.round(fontSizeTable7) + "px");
    $("#t7t2").css('font-size', Math.round(fontSizeTable7) + "px");
    $("#t7t3").css('font-size', Math.round(fontSizeTable7) + "px");
    $("#t7t4").css('font-size', Math.round(fontSizeTable7) + "px");
    $("#t7t5").css('font-size', Math.round(fontSizeTable7) + "px");

    var fontSizeTable71 = w * 0.0182;
    $("#tablica_s7_text").css('font-size', Math.round(fontSizeTable71) + "px");

    $("#akm_obl_text").css('font-size', Math.round(fontSizeAkmObl) + "px");
    $("#kar_obl_text").css('font-size', Math.round(fontSizeKarObl) + "px");

    $("#zaglavie_text").css('font-size', Math.round(fontSize) + "px");

    $("#table_text_1_1").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_2_1").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_3_1").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_4_1").css('font-size', Math.round(fontSize2) + "px");

    $("#table_text_1_2").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_2_2").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_3_2").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_4_2").css('font-size', Math.round(fontSize2) + "px");

    $("#table_text_1_3").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_2_3").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_3_3").css('font-size', Math.round(fontSize2) + "px");
    $("#table_text_4_4").css('font-size', Math.round(fontSize2) + "px");

    var fontSize2_1 = w * 0.013;
    $("#table_text_1").css('font-size', Math.round(fontSize2_1) + "px");
    $("#table_text_2").css('font-size', Math.round(fontSize2_1) + "px");
    $("#table_text_3").css('font-size', Math.round(fontSize2_1) + "px");
    $("#table_text_4").css('font-size', Math.round(fontSize2_1) + "px");

    $("#table_input_1").css('font-size', Math.round(fontSize2) + "px");
    $("#table_input_2").css('font-size', Math.round(fontSize2) + "px");
    $("#table_input_3").css('font-size', Math.round(fontSize2) + "px");
    $("#table_input_4").css('font-size', Math.round(fontSize2) + "px");

    $("#p_text_s5").css('font-size', Math.round(fontSize3) + "px");

    var fontSize444 = w * 0.015;
    $("#otvet_1_s7").css('font-size', Math.round(fontSize444) + "px");
    $("#otvet_2_s7").css('font-size', Math.round(fontSize444) + "px");
    $("#otvet_3_s7").css('font-size', Math.round(fontSize444) + "px");
    $("#otvet_4_s7").css('font-size', Math.round(fontSize444) + "px");

    $("#table_input_1_s12").css('font-size', Math.round(fontSize4) + "px");

    var fontSize5 = w * 0.018;
    $("#formula_number_11_s14").css('font-size', Math.round(fontSize5) + "px");
    $("#formula_number_12_s14").css('font-size', Math.round(fontSize5) + "px");

    $("#formula_number_21_s14").css('font-size', Math.round(fontSize5) + "px");
    $("#formula_number_22_s14").css('font-size', Math.round(fontSize5) + "px");

    $("#formula_number_31_s14").css('font-size', Math.round(fontSize5) + "px");
    $("#formula_number_32_s14").css('font-size', Math.round(fontSize5) + "px");

    $("#formula_number_41_s14").css('font-size', Math.round(fontSize5) + "px");
    $("#formula_number_42_s14").css('font-size', Math.round(fontSize5) + "px");

    var fontsize6 = w * 0.018;
    $("#texts17_1").css('font-size', Math.round(fontsize6) + "px");
    $("#input1_s17").css('font-size', Math.round(fontsize6) + "px");
    $("#texts17_3").css('font-size', Math.round(fontsize6) + "px");

    $("#texts2_17_1").css('font-size', Math.round(fontsize6) + "px");
    $("#input2_s17").css('font-size', Math.round(fontsize6) + "px");
    $("#texts2_17_3").css('font-size', Math.round(fontsize6) + "px");

    $(".classOfThirdInputAndTexts").css('font-size', Math.round(fontsize6) + "px");

    var fontSize7 = w * 0.018;
    $("#tur_number").css('font-size', Math.round(fontSize7) + "px");
    $("#marks_pole").css('font-size', Math.round(fontSize7) + "px");

    var fontSize8 = w * 0.018;
    $("#task_text_s12").css('font-size', Math.round(fontSize7) + "px");
    var fontSize8 = w * 0.018;

    for (var i = 1; i <= 9; ++i) {

    }
    $("#tur1cardslettertext" + i).css('font-size', Math.round(fontSize8) + "px");
    var fontSize9 = w * 0.016;
    $("#tablica_text").css('font-size', Math.round(fontSize9) + "px");

    var fontSize10 = w * 0.018;
    $("#task_text_s8").css('font-size', Math.round(fontSize10) + "px");

    var fontSize11 = w * 0.02;
    $("#right_answer_s10").css('font-size', Math.round(fontSize10) + "px");
};

app.controller('GameController', ['$scope', function($scope) {

    $scope.playSound = function(num, func) {
        sounds[num - 1].play();
        if (func != undefined && func != null) {
            sounds[num - 1].addEventListener("ended", function () {
                func();
            });
        }
    };

    $scope.dragEnd1 = function(instance, event, pointer, itemName) {
        var element = $(instance.element);
        var init = {
            "top": "",
            "left": ""
        };

    };

    $scope.step8Text = "РАССМОТРИ ИЗДЕЛИЯ, СОДЕРЖАЩИЕ СОЕДИНЕНИЯ МЕТАЛЛОВ, РАСПРОСТРАНЕННЫХ НА ТЕРРИТОРИИ ВОСТОЧНО-КАЗАХСТАНСКОЙ ОБЛАСТИ. " +
        "ПРИКОСНУВШИСЬ К ЯЧЕЙКЕ С ИЗДЕЛИЕМ, ПОСМОТРИ ФОРМУЛУ ХИМИЧЕСКОГО СОЕДИНЕНИЯ.";

    $scope.step8TextFill = '';

    $scope.showTaskPole = false;

    $scope.taskTextPole = [
        "Этот вид «молока» не имеет способности скисать, но имеет удивительную способностьповиснуть на стене. Когда речь заходит о нем, часто говорят гасили то, что не горело. Является реактивом для качественного определения углекислого газа. Его относительная молекулярная масса равна 74",
        "Это соединение зачастую называют «хлебом химической индустрии». Относится к классу кислот. Содержит в своем составе индекс, раный двум и четырем.",
        "Про этот элемент можно сказать, что он в холод прячется в нору, а поднимается в жару.",
        "Формула одного из его оксидов ртути содержит  в своем составе индекс, равный двум. Чему равна молекулярная масса этого оксида?"
    ];

    $scope.checkButtonClickedPole = function() {
        $scope.titulkaShowing = true;
        $scope.showPoleChudes = false;
    }
    $scope.toTheBeginning = function() {
        $scope.titulkaShowing = true;
        $scope.mainTsorShowing = false;
    }
    $scope.redButtonPressed = false;
    $scope.marksPole = 0;
    $scope.turNumber = 0;
    $scope.turText = ['ТУР 1','ТУР 2','ТУР 3 (1)', 'ТУР 3 (2)'];
    $scope.titulkaHighlighted1 = false;
    $scope.titulkaHighlighted2 = false;
    $scope.titulkaHighlighted3 = false;

    $scope.titulkaShowing = true;
    $scope.mainTsorShowing = false;
    $scope.flagsHighlighted1 = false;
    $scope.showAllFlags = false;
    $scope.showAktobeFlag = false;
    $scope.stepNum = 1;

    $scope.tableTextColumn1 = ['x','y','z','v'];

    $scope.rightAnswers = ['50', '52', '53', '54'];

    $scope.input1S4 = $("#table_input_1");
    $scope.input2S4 = $("#table_input_2");
    $scope.input3S4 = $("#table_input_3");
    $scope.input4S4 = $("#table_input_4");

    $scope.input1S7 = $("#otvet_1_s7");
    $scope.input2S7 = $("#otvet_2_s7");
    $scope.input3S7 = $("#otvet_3_s7");
    $scope.input4S7 = $("#otvet_4_s7");

    $scope.inputS12 = $("#table_input_1_s12");

    $scope.input11S14 = $("#formula_text_11_s14");
    $scope.input12S14 = $("#formula_text_12_s14");
    $scope.input21S14 = $("#formula_text_21_s14");
    $scope.input22S14 = $("#formula_text_22_s14");
    $scope.input31S14 = $("#formula_text_31_s14");
    $scope.input32S14 = $("#formula_text_32_s14");
    $scope.input41S14 = $("#formula_text_41_s14");
    $scope.input42S14 = $("#formula_text_42_s14");

    $scope.checkButtonPressedS4 = false;

    $scope.showCheckButtonS7 = false;

    $scope.barabanAllowed = true;
    $scope.currentPointsToWin = 0;
    $scope.numbersOfWheel = [20, 50, 100];
    $scope.redButtonClick = function() {
        // todo
        if ($scope.barabanAllowed) {
            $scope.showInstructionPole = false;
            $scope.showTaskPole = true;
            $scope.wheel.animateSprite('play', 'run');
            setTimeout(function() {
                $scope.$apply(function() {
                    //
                    $scope.wheel.animateSprite('stop', 'run');
                    var number = Math.round((Math.random() * 100)) % 3;
                    console.log("frame is ", number)
                    $scope.wheel.animateSprite('frame', number);

                    $scope.showTaskPole = true;
                    $scope.showTick = false;
                    $scope.turNumber++;

                    $scope.currentPointsToWin = $scope.numbersOfWheel[number];
                    $scope.barabanAllowed = false;
                });
            }, 2000)
        }
    }
    $scope.redButtonPressStart = function() {
        $scope.redButtonPressed = true;
    }

    $scope.redButtonPressEnd = function() {
        $scope.redButtonPressed = false;
    }
    $scope.bgColorCheck = function(num) {

        if (!$scope.checkButtonPressedS4) {
            return '';
        }

        var initialValues = ['x','y','z','v'];

        if ($scope.showFontBoldS4) {
            if (!$scope.answerIsCorrectS4[num - 1]) {
                return 'background_color_green bold_font';
            }
        }
        if ($scope.tableTextColumn1[num - 1] === $scope.rightAnswers[num - 1]) {
            $("#table_input_" + (num - 1)).disabled = true;
            return 'background_color_green';
        } else if ($scope.tableTextColumn1[num - 1] === initialValues[num - 1]) {
            return 'background_color_red';
        } else {
            return 'background_color_red';
        }
    }

    $scope.bgColorCheckS7 = function(num) {

        if (!$scope.checkButtonPressedS7) {
            return '';
        }

        if ($scope.showFontBoldS7) {
            if (!$scope.answerIsCorrectS7[num - 1]) {
                return 'background_color_green bold_font';
            }
        }
        if ($scope.answerTextS7[num - 1] === $scope.rightAnswersS7[num - 1]) {
//            $("#table_input_" + (num - 1)).disabled = true;
            return 'background_color_green';
        } else {
            return 'background_color_red';
        }
    }

    $scope.bgColorCheckS14 = function(n1, n2) {

        if (!$scope.checkButtonPressedS14) {
            return '';
        }

        if ($scope.showFontBoldS14) {
            if (!$scope.answerIsCorrectS14[(n1 - 1) * 2 + (n2 - 1)]) {
                return 'background_color_green bold_font';
            }
        }

        if ($scope.answerIsCorrectS14[(n1 - 1) * 2 + (n2 - 1)]) {
            return 'background_color_green';
        } else {
            return 'background_color_red';
        }
        return 'background_color_green';
    }
    $scope.bgColorCheckFS14 = function(num) {

        if (!$scope.checkButtonPressedS14) {
            return '';
        }
        if ($scope.showFontBoldS14) {
            if (!$scope.answerIsCorrectFS14[num]) {
                return 'background_color_green bold_font';
            }
        }

        if ($scope.answerIsCorrectFS14[num]) {
            return 'background_color_green';
        } else {
            return 'background_color_red';
        }
        return 'background_color_green';
    }
    $scope.tableTextCol1 = function(ind) {
        return $scope.tableTextColumn1[ind - 1];
    }
    $scope.turTaskImgClass = function() {
        return 'p' + $scope.turNumber + "_tur";
    }
    $scope.titulkaClass = function(num) {
        if (num == 1) {
            if ($scope.titulkaHighlighted1) {
                return 'tit_highlighted1';
            } else {
                return 'tit1';
            }
        } else if (num == 2) {
            if ($scope.titulkaHighlighted2) {
                return 'tit_highlighted2';
            } else {
                return 'tit2';
            }
        } else if (num == 3) {
            if ($scope.titulkaHighlighted3) {
                return 'tit_highlighted3';
            } else {
                return 'tit3';
            }
        }
    }

    $scope.titulkaMouseOver = function(num) {
        if (num == 1) {
            $scope.titulkaHighlighted1 = true;
        } else if (num == 2) {
            $scope.titulkaHighlighted2 = true;
        } else if (num == 3) {
            $scope.titulkaHighlighted3 = true;
        }
    }

    $scope.titulkaMouseLeave = function(num) {
        if (num == 1) {
            $scope.titulkaHighlighted1 = false;
        } else if (num == 2) {
            $scope.titulkaHighlighted2 = false;
        } else if (num == 3) {
            $scope.titulkaHighlighted3 = false;
        }
    }

    $scope.titulkaClick = function(num) {
        if (num == 1) {
            $scope.showMain();
        } else if (num == 2) {
            $scope.showPole();
        } else if (num == 3) {
            $scope.showIstochniki();
        }
    }

    $scope.showPoleChudes = false;
    $scope.zastavka = true;
    $scope.showCheckButtonPole = false;
    $scope.showPole = function() {
        $scope.turNumber = 0;
        $scope.titulkaShowing = false;
        $scope.showingIstochniki = false;
        $scope.showPoleChudes = true;
        $scope.showCheckButtonPole = false;
    }
    $scope.showIstochniki = function() {
        $scope.titulkaShowing = false;
        $scope.showPoleChudes = false;
        $scope.showingIstochniki = true;
    }
    $scope.showMain = function() {
        $scope.titulkaShowing = false;
        $scope.showingIstochniki = false;
        $scope.mainTsorShowing = true;
//        $scope.giveCoin(null);
        $scope.startStep1();
    }


	$scope.showHeli1 = false;
    $scope.startStep1 = function() {
        
		$scope.showHeli1 = false;
		$scope.playSound(1);
        $scope.stepNum = 1;
        var limit = 10;
        var count = 0;

        var highlight = function() {
            if (count == limit) {
                return;
            }
            count++;
            $scope.$apply(function() {
                $scope.flagsHighlighted1 = !$scope.flagsHighlighted1;
            });
            setTimeout(highlight, 200);
        }

        setTimeout(highlight, 200);

        $scope.showAllFlags = true;

        $scope.flagsHighlighted1 = false; 
		
        setTimeout(function() {
            $scope.$apply(function() {
//                $scope.startStep2();
            });
        }, 5000);
    }

    $scope.mainTsorClass = function() {
        return 'main_class_' + $scope.stepNum;
    }

    $scope.stopSounds = function() {
        sounds.forEach(function(i) {
            i.pause();
            i.currentTime = 0;
        });
    }

    $scope.nextButtonClick = function() {
        $scope.stopSounds();
        if ($scope.stepNum === 1) {
            $scope.startStep2();
        } else if ($scope.stepNum === 2) {
            $scope.startStep3();
        } else if ($scope.stepNum === 3) {
            $scope.startStep4();
        } else if ($scope.stepNum === 4) {
            $scope.startStep5();
        } else if ($scope.stepNum === 5) {
            $scope.startStep6();
        } else if ($scope.stepNum === 6) {
            $scope.startStep7();
        } else if ($scope.stepNum === 7) {
            $scope.startStep8();
        } else if ($scope.stepNum === 8) {
            $scope.startStep9();
        } else if ($scope.stepNum === 9) {
            $scope.startStep10();
        } else if ($scope.stepNum === 10) {
            $scope.startStep11();
        } else if ($scope.stepNum === 11) {
            $scope.startStep12();
        } else if ($scope.stepNum === 12) {
            $scope.startStep13();
        } else if ($scope.stepNum === 13) {
            $scope.startStep14();
        } else if ($scope.stepNum === 14) {
            $scope.startStep15();
        } else if ($scope.stepNum === 15) {
            $scope.startStep16();
        } else if ($scope.stepNum === 16) {
            $scope.startStep17();
        }
    }
    $scope.backButtonClick = function() {
        if ($scope.stepNum === 2) {
            $scope.startStep1();
        } else if ($scope.stepNum === 3) {
            $scope.startStep2();
        } else if ($scope.stepNum === 4) {
            $scope.startStep3();
        } else if ($scope.stepNum === 5) {
            $scope.startStep4();
        } else if ($scope.stepNum === 6) {
            $scope.startStep5();
        } else if ($scope.stepNum === 7) {
            $scope.startStep6();
        } else if ($scope.stepNum === 8) {
            $scope.startStep7();
        } else if ($scope.stepNum === 9) {
            $scope.startStep8();
        } else if ($scope.stepNum === 10) {
            $scope.startStep9();
        } else if ($scope.stepNum === 11) {
            $scope.startStep10();
        } else if ($scope.stepNum === 12) {
            $scope.startStep11();
        } else if ($scope.stepNum === 13) {
            $scope.startStep12();
        } else if ($scope.stepNum === 14) {
            $scope.startStep13();
        } else if ($scope.stepNum === 15) {
            $scope.startStep14();
        } else if ($scope.stepNum === 16) {
            $scope.startStep15();
        } else if ($scope.stepNum === 17) {
            $scope.startStep16();
        }
    }
    $scope.heli2Show = false;
    $scope.heli3Show = false;

    $scope.startStep2 = function() {
        // todo
		$scope.showHeli1 = true;     
		$scope.stepNum = 2;
        $scope.showAllFlags = false;
        $scope.showAktobeFlag = true;
        $scope.playSound(2);

		var settings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 12,
            rows: 2,
            animations: {
                run: [0, 1, 2,3,4,5,6,7,8,9,10,11]
            },
            complete: function(){
                $scope.$apply(function() {
					$scope.startWordsStep3();
				});
            }
        };


        $scope.heli1.animateSprite(settings);
        $scope.heli1.animateSprite('frame', '0');

        setTimeout(function() {
            $scope.$apply(function() {
//                $scope.startStep3();
            });
        }, 5000);
    }

    $scope.heliClass = 1;
    $scope.startStep3 = function() {
        // todo
        $scope.stepNum = 3;
		$scope.showHeli1 = true; 
        $scope.showAllFlags = false;
        $scope.showAktobeFlag = true;
        $scope.heli2Show = false;
        $scope.heli3Show = true;

        var newpos = {
            left: '18.85%',
            top: '38.4%'
        }
        var init = {
            top: '',
            left: ''
        } 
        $scope.stroka5S3.css(init);
        $scope.stroka4S3.css(init);
        $scope.stroka3S3.css(init);
        $scope.stroka2S3.css(init);
        $scope.stroka1S3.css(init);

        $scope.heli1.animateSprite('restart', 'run');
        $scope.playSound(3);

        setTimeout(function() {
            
        }, 8000);
    }

    $scope.startWordsStep3 = function() {
        // todo
        var dur = 4000;
        var word5 = function() {
            $scope.stroka5S3.animate({"left": "28.9%"}, {duration: dur, queue:false, complete: function() {
                //
            }});
        }
        var word4 = function() {
            $scope.stroka4S3.animate({"left": "0.25%"}, {duration: dur, queue:false, complete: function() {
                word5();
            }});
        }
        var word3 = function() {
            $scope.stroka3S3.animate({"left": "25.25%"}, {duration: dur, queue:false, complete: function() {
                word4();
            }});
        }
        var word2 = function() {
            $scope.stroka2S3.animate({"left": "4.55%"}, {duration: dur, queue:false, complete: function() {
                word3();
            }});
        }
        var word1 = function() {
            $scope.stroka1S3.animate({"left": "22.6%"}, {duration: dur, queue:false, complete: function() {
                word2();
            }});
        }

        word1();
    }

    $scope.startStep4 = function() {
        // todo
		$scope.showHeli1 = false; 
        $scope.stepNum = 4;
        $scope.showAktobeFlag = false;
        $scope.heli3Show = false;
    }

    $scope.stroka1S3 = $("#stroka_1");
    $scope.stroka2S3 = $("#stroka_2");
    $scope.stroka3S3 = $("#stroka_3");
    $scope.stroka4S3 = $("#stroka_4");
    $scope.stroka5S3 = $("#stroka_5");

    $scope.stroka1S15 = $("#stroka15_1");
    $scope.stroka2S15 = $("#stroka15_2");
    $scope.stroka3S15 = $("#stroka15_3");
    $scope.stroka4S15 = $("#stroka15_4");
    $scope.stroka5S15 = $("#stroka15_5");

    $scope.showCheckButtonS4 = false;

    $scope.answerIsCorrectS4 = [false,false,false,false];

    $scope.showFontBoldS4 = false;

    $scope.checkButtonClickedS4 = function() {
        $scope.checkButtonPressedS4 = true;
        setTimeout(function() {
            $scope.$apply(function() {
                $scope.changeToCorrectAnswersS4();
                if ($scope.answerIsCorrectS4[0] && $scope.answerIsCorrectS4[1] &&
                    $scope.answerIsCorrectS4[2] && $scope.answerIsCorrectS4[3]) {
                    $scope.giveCoin(function() {
//                        $scope.startStep5();
                    });
                } else {
                    setTimeout(function() {
                        $scope.$apply(function() {
//                            $scope.startStep5();
                        });
                    }, 5000);
                }
            });
        }, 3000);
    };

    $scope.lampHighlighted = false;
    $scope.lampClicked = function() {
        $scope.nextButtonClick();
    }

    $scope.tableMendShow17 = false;
    $scope.lampClickedS17 = function() {
        $scope.tableMendShow17 = !$scope.tableMendShow17;
    }

    $scope.coinNumber = [0,0,0,0,0,0];

    $scope.showEmblemGiveCoin = [false,false,false,false,false,false,false];
    $scope.tableFilledShowS14 = false;
    $scope.giveCoin = function(clb) {
        // todo
        if ($scope.stepNum < 7) {
            $scope.coinNumber[0]++;
            $scope.showEmblemGiveCoin[0] = true;
            $("#aktobe_icon1").switchClass("icon_pos_init", "icon_pos_1", 3000);
        } else if ($scope.stepNum == 7) {
            $scope.coinNumber[0]++;
            $scope.showEmblemGiveCoin[1] = true;
            $("#aktobe_icon2").switchClass("icon_pos_init", "icon_pos_2", 3000);
        } else if ($scope.stepNum == 10) {
            $scope.coinNumber[1]++;
            $scope.showEmblemGiveCoin[2] = true;
            $("#aktobe_icon3").switchClass("icon_pos_init", "icon_pos_3", 3000);
        } else if ($scope.stepNum == 12) {
            $scope.coinNumber[2]++;
            $scope.showEmblemGiveCoin[3] = true;
            $("#aktobe_icon4").switchClass("icon_pos_init", "icon_pos_4", 3000);
        }

        if (clb != undefined && clb != null) {
            clb();
        }
    }

    $scope.startStep5 = function() {
        $scope.stepNum = 5;
        setTimeout(function() {
//            $scope.startStep6();
        }, 5000);
    }

    $scope.crSignShowS6 = false;

    $scope.startStep6 = function() {
        // todo
        $scope.stepNum = 6;

        var count = 0;
        var limit = 20;

        var highlight = function() {
            if (count == limit) {
                return;
            }

            ++count;

            $scope.$apply(function() {
                $scope.crSignShowS6 = !$scope.crSignShowS6;
            });

            setTimeout(function() {
                highlight();
            }, 500);
        }

        setTimeout(function() {
            highlight();
        }, 2000);

    }

    $scope.minimizeClickedS6 = function() {
//        $scope.startStep7();
    }

    $scope.startStep7 = function() {
        $scope.tableFilledShowS7 = false;
        $scope.stepNum = 7;

        $scope.showFontBoldS7 = false;
        $scope.checkButtonPressedS7 = false;
        $(".inputs7").val("");
        var count = 0;
        var limit = 10;
        var dur = 500;
        var highlight = function() {
            $scope.$apply(function(){
                if (count <= limit) {
                    ++count;
                    $scope.lampHighlighted = !$scope.lampHighlighted;
                    setTimeout(highlight, dur);
                } else {
                    $scope.lampHighlighted = false;
                }
            });
        }

        setTimeout(highlight, 1000);
        // todo
        $scope.playSound(4, function() {

        });
    }

    $scope.changeToCorrectAnswersS4 = function() {
        for (var i = 0; i < 4; ++i) {
            $scope.answerIsCorrectS4[i] = $scope.tableTextColumn1[i] === $scope.rightAnswers[i];
        }

        $scope.input1S4.val($scope.rightAnswers[0]);
        $scope.input2S4.val($scope.rightAnswers[1]);
        $scope.input3S4.val($scope.rightAnswers[2]);
        $scope.input4S4.val($scope.rightAnswers[3]);

        $scope.showFontBoldS4 = true;
    }

    $scope.answerIsCorrectS14 = [   false, false, false, false,
        false, false, false, false];

    $scope.rightAnswersS14 = ['C', 'O', 'SI', 'O', 'N', 'H', 'NA', 'CL'];
    $scope.answerIsCorrectFS14 = [false, false, false, false, false, false, false, false];

    $scope.showFontBoldS14 = false;

    $scope.changeToCorrectAnswersS14 = function() {

        $scope.answerIsCorrectFS14[0] = $scope.answer11FS14.text() === '0';
        $scope.answerIsCorrectFS14[1] = $scope.answer12FS14.text().trim() === '2';
        $scope.answerIsCorrectFS14[2] = $scope.answer21FS14.text().trim() === '0';
        $scope.answerIsCorrectFS14[3] = $scope.answer22FS14.text().trim() === '2';
        $scope.answerIsCorrectFS14[4] = $scope.answer31FS14.text().trim() === '0';
        $scope.answerIsCorrectFS14[5] = $scope.answer32FS14.text().trim() === '3';
        $scope.answerIsCorrectFS14[6] = $scope.answer41FS14.text().trim() === '0';
        $scope.answerIsCorrectFS14[7] = $scope.answer41FS14.text().trim() === '0';

        $scope.answerIsCorrectS14[0] = $scope.answer11S14 === $scope.rightAnswersS14[0];
        $scope.answerIsCorrectS14[1] = $scope.answer12S14 === $scope.rightAnswersS14[1];
        $scope.answerIsCorrectS14[2] = $scope.answer21S14 === $scope.rightAnswersS14[2];
        $scope.answerIsCorrectS14[3] = $scope.answer22S14 === $scope.rightAnswersS14[3];
        $scope.answerIsCorrectS14[4] = $scope.answer31S14 === $scope.rightAnswersS14[4];
        $scope.answerIsCorrectS14[5] = $scope.answer32S14 === $scope.rightAnswersS14[5];
        $scope.answerIsCorrectS14[6] = $scope.answer41S14 === $scope.rightAnswersS14[6];
        $scope.answerIsCorrectS14[7] = $scope.answer42S14 === $scope.rightAnswersS14[7];

        $scope.input11S14.val($scope.rightAnswersS14[0]);
        $scope.input12S14.val($scope.rightAnswersS14[1]);
        $scope.input21S14.val($scope.rightAnswersS14[2]);
        $scope.input22S14.val($scope.rightAnswersS14[3]);
        $scope.input31S14.val($scope.rightAnswersS14[4]);
        $scope.input32S14.val($scope.rightAnswersS14[5]);
        $scope.input41S14.val($scope.rightAnswersS14[6]);
        $scope.input42S14.val($scope.rightAnswersS14[7]);

        $scope.answer11FS14.text('0');
        $scope.answer12FS14.text('2');
        $scope.answer21FS14.text('0');
        $scope.answer22FS14.text('2');
        $scope.answer31FS14.text('0');
        $scope.answer32FS14.text('3');
        $scope.answer41FS14.text('0');
        $scope.answer42FS14.text('0');

        $scope.showFontBoldS14 = true;
    }

    $scope.answerIsCorrectS7 = [false,false,false,false];

    $scope.showFontBoldS7 = false;
    $scope.rightAnswersS7 = ['0.0431','0.8776','0.0955','0.0238'];
    $scope.answerTextS7 = ['', '', '', ''];

    $scope.changeToCorrectAnswersS7 = function() {
        for (var i = 0; i < 4; ++i) {
            $scope.answerIsCorrectS7[i] = $scope.answerTextS7[i] === $scope.rightAnswersS7[i];
        }

        $scope.input1S7.val($scope.rightAnswersS7[0]);
        $scope.input2S7.val($scope.rightAnswersS7[1]);
        $scope.input3S7.val($scope.rightAnswersS7[2]);
        $scope.input4S7.val($scope.rightAnswersS7[3]);

        $scope.showFontBoldS7 = true;
    }

    $scope.checkButtonClickedS7 = function() {
        $scope.checkButtonPressedS7 = true;
        setTimeout(function() {
            $scope.$apply(function() {
                $scope.changeToCorrectAnswersS7();
                if ($scope.answerIsCorrectS7[0] && $scope.answerIsCorrectS7[1] &&
                    $scope.answerIsCorrectS7[2] && $scope.answerIsCorrectS7[3]) {
                    $scope.giveCoin(function() {
                        $scope.wonTicketToVKO();

                    });
                } else {
                    setTimeout(function() {
                        $scope.$apply(function() {
//                            $scope.startStep8();
                        });
                    }, 5000);
                }
            });
        }, 3000);
    };

    $scope.checkButtonClickedS17 = function() {
        $scope.checkButtonPressedS17 = true;
        setTimeout(function() {
            $scope.$apply(function() {
                $scope.changeToCorrectAnswersS17();
                if ($scope.answer1S17 === $scope.correctAnswers1S17 &&
                    $scope.answer2S17 === $scope.correctAnswers2S17 &&
                    $scope.answer31S17 === $scope.correctAnswers3S17[0] &&
                    $scope.answer32S17 === $scope.correctAnswers3S17[1] &&
                    $scope.answer33S17 === $scope.correctAnswers3S17[2] &&
                    $scope.answer34S17 === $scope.correctAnswers3S17[3] &&
                    $scope.answer35S17 === $scope.correctAnswers3S17[4]) {
                    $scope.giveCoin(function() {
                        $scope.wonTicketToVKO();

                    });
                } else {
                    setTimeout(function() {
                        $scope.$apply(function() {
                            // todo
                        });
                    }, 5000);
                }
            });
        }, 3000);
    };

    $scope.congratsShowS7 = false;
    $scope.wonTicketToVKO = function() {
        $scope.congratsShowS7 = true;
        setTimeout(function() {
            $scope.$apply(function() {
//                $scope.startStep8();
            });
        }, 2000);
    }

    $scope.showElementS8 = 0;

    $scope.setTextS8 = function(txt) {
        $scope.step8TextFill = txt;
    }
    $scope.startTextFillingS8 = function() {

        var currentChar = 0;
        $scope.setTextS8('');
        var txt = '';
        var deltaT = 100;
        var filler = function() {
            if (currentChar == $scope.step8Text.length) {
                return;
            }
            txt += $scope.step8Text[currentChar];
            $scope.setTextS8(txt);
            ++currentChar;
            setTimeout(filler, deltaT);
        }
        setTimeout(filler, deltaT);
    }
    $scope.showElementsS8 = true;
    $scope.startStep8 = function() {
        // todo
        $scope.showElementsS8 = true;
        $scope.checkStatus[0] = 0;
        $scope.checkStatus[1] = 0;
        $scope.checkStatus[2] = 0;
        $scope.stepNum = 8;
        $scope.playSound(5);

        $scope.startTextFillingS8();
        var timing = [2000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000, 1000];

        var incr = function() {
            if ($scope.showElementS8 == 10) {
                // todo remove this
                $scope.$apply(function() {
//                    $scope.startStep9();
                    $scope.showElementsS8 = false;
                });
                return;
            }

            $scope.$apply(function() {
                $scope.showElementS8++;
            });

            console.log($scope.showElementS8);
            setTimeout(incr, timing[$scope.showElementS8]);
        }
        setTimeout(incr, timing[$scope.showElementS8]);
    }

    $scope.cardStatusesS9 = {   'pb': false, 'zn': false, 'cu':false, 'au': false, 'ag':false, 'cd': false, 'bi':false,
        'sn':false, 'in':false, 'ta':false};
    $scope.elementClickedS9 = function(tx) {
        $scope.cardStatusesS9[tx] = !$scope.cardStatusesS9[tx];
    }
    $scope.classOfElementS9 = function(tx) {
        if ($scope.cardStatusesS9[tx]) {
            return 'black_bg';
        } else {
            return 'opacity0';
        }
    };

    $scope.startStep9 = function() {
        // todo
        $scope.showElementS8 = 10;
        $scope.stepNum = 9;
        setTimeout(function() {
            $scope.$apply(function() {
//                $scope.startStep10();
            })
        }, 5000);
    }

    $scope.testAnsT1S10 = '';
    $scope.testAnsT2S10 = '';
    $scope.testAnsT3S10 = '';
    $scope.testAnsT1Dirty = false;
    $scope.testAnsT2Dirty = false;
    $scope.testAnsT3Dirty = false;
    $scope.checkButtonPressedS10 = false;
    $scope.rightAnswersS10 = ['b', 'a', 'a'];
    $scope.taskDoneS10 = false;

    $scope.showWonTextS10 = function() {
        $scope.taskDoneS10 = true;
        setTimeout(function() {
            $scope.$apply(function() {
//                $scope.startStep11();
            });
        }, 2000);

    }
    $scope.checkStatus = [0, 0, 0];
    $scope.checkButtonClickedS10 = function() {
        $scope.checkButtonPressedS10 = true;

        if ($scope.rightAnswersS10[0] == $scope.testAnsT1S10) {
            $scope.checkStatus[0] = 1;
        } else {
            $scope.checkStatus[0] = 2;
        }
        if ($scope.rightAnswersS10[1] == $scope.testAnsT2S10) {
            $scope.checkStatus[1] = 1;
        } else {
            $scope.checkStatus[1] = 2;
        }
        if ($scope.rightAnswersS10[2] == $scope.testAnsT3S10) {
            $scope.checkStatus[2] = 1;
        } else {
            $scope.checkStatus[2] = 2;
        }

        setTimeout(function() {
            $scope.$apply(function() {
                if ($scope.rightAnswersS10[0] == $scope.testAnsT1S10 &&
                    $scope.rightAnswersS10[1] == $scope.testAnsT2S10 &&
                    $scope.rightAnswersS10[2] == $scope.testAnsT3S10) {
                    $scope.giveCoin(function() {
                        $scope.showWonTextS10();
                    });
                } else {
                    setTimeout(function() {
                        $scope.$apply(function() {
                            $scope.showRightAnswerS10 = true;
                            setTimeout(function() {
                                $scope.$apply(function() {
//                                    $scope.startStep11();
                                });
                            }, 3000);
                        });
                    }, 200);
                }
            });
        }, 3000);
    }

    $scope.startStep10 = function() {
        // todo
        $scope.stepNum = 10;
        $scope.checkStatus[0] = 0;
        $scope.checkStatus[1] = 0;
        $scope.checkStatus[2] = 0;
        $(".radios").prop('checked', false);
    }

    $scope.startStep11 = function() {
        // todo
        $scope.stepNum = 11;
        $scope.playSound(6);
        setTimeout(function() {
            $scope.$apply(function() {
//                $scope.startStep12();
            })
        }, 2000);
    }

    $scope.showCheckButtonS12 = false;
    $scope.checkButtonPressedS12 = false;
    $scope.rightAnswerS12 = '55.847';
    $scope.answerS12 = '';
    $scope.answerIsCorrectS12 = false;
    $scope.showFontBoldS12 = false;

    $scope.changeToCorrectAnswersS12 = function() {
        $scope.answerIsCorrectS12 = $scope.answerS12 === $scope.rightAnswerS12;
        $scope.inputS12.val($scope.rightAnswerS12);
        $scope.showFontBoldS12 = true;
    }

    $scope.checkButtonClickedS12 = function() {
        $scope.checkButtonPressedS12 = true;
        setTimeout(function() {
            $scope.$apply(function() {
                $scope.changeToCorrectAnswersS12();
                if ($scope.answerIsCorrectS12) {
                    $scope.giveCoin(function() {
//                        $scope.startStep13();
                    });
                } else {
                    setTimeout(function() {
                        $scope.$apply(function() {
//                            $scope.startStep13();
                        });
                    }, 5000);
                }
            });
        }, 3000);
    };

    $scope.bgColorCheckS12 = function() {
        if (!$scope.checkButtonPressedS12) {
            return '';
        }
        if ($scope.showFontBoldS12) {
            if (!$scope.answerIsCorrectS12) {
                return 'background_color_green bold_font';
            }
        }
        if ($scope.answerS12 === $scope.rightAnswerS12) {
//            $("#table_input_" + (num - 1)).disabled = true;
            return 'background_color_green';
        } else {
            return 'background_color_red';
        }
    }

    $scope.startStep12 = function() {
        $scope.stepNum = 12;
    }

    $scope.showAlS13 = false;
    $scope.showOS13 = false;
    $scope.formulaShow1 = false;
    $scope.formulaShow2 = false;

    $scope.sprite4S13Show = false;
    $scope.sprite3S13Show = false;
    $scope.sprite2S13Show = false;

    $scope.startStep13 = function() {
        $scope.stepNum = 13;
        $scope.playSound(7);
        $scope.sprite4S13Show = false;
        $scope.sprite3S13Show = false;
        $scope.sprite2S13Show = false;

        $scope.sprite1S13.animateSprite('frame', 0);
        $scope.sprite2S13.animateSprite('frame', 0);
        $scope.sprite3S13.animateSprite('frame', 0);
        $scope.sprite4S13.animateSprite('frame', 0);

        $scope.showOS13 = false;
        $scope.showAlS13 = false;

        setTimeout(function() {
            $scope.sprite1S13.animateSprite('restart', 'run');
        }, 2000);

//        setTimeout(function() {
//            $scope.$apply(function() {$scope.showAlS13 = true;});
//        }, 500);
//        setTimeout(function() {
//            $scope.$apply(function() {$scope.showOS13 = true;});
//        }, 1000);
//
//        setTimeout(function() {
//            $scope.$apply(function() {$scope.formulaShow1 = true;});
//        }, 1500);
//        setTimeout(function() {
//            $scope.$apply(function() {$scope.formulaShow2 = true;});
//        }, 2000);
//        setTimeout(function() {
////            $scope.$apply(function() {$scope.startStep14()});
//        }, 2500);
    }

    $scope.formulaNumVals = [1, 1, 1, 1, 1, 1, 1, 1];
    $scope.upClick = function(num) {
        $scope.formulaNumVals[num - 1]++;
    }

    $scope.downClick = function(num) {
        if ($scope.formulaNumVals[num - 1] > 1) {
            $scope.formulaNumVals[num - 1]--;
        }
    }

    $scope.startStep14 = function() {
        // todo
        $scope.stepNum = 14;
    }

    $scope.heli1 = $("#heli1");
    $scope.heli15_1Show = false;
    $scope.heli15_2Show = false;
    $scope.heliClass15_2 = 1;
    $scope.startStep15 = function() {
        $scope.stepNum = 15;

        var newpos = {
            left: '38.85%',
            top: '48.4%'
        }
        var init = {
            top: '',
            left: ''
        }  
        $scope.stroka1S15.css(init);
        $scope.stroka2S15.css(init);
        $scope.stroka3S15.css(init);
        $scope.stroka4S15.css(init);
        $scope.stroka5S15.css(init);

		var settings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 11,
            rows: 2,
            animations: {
                run: [0, 1, 2,3,4,5,6,7,8,9,10 ]
            },
            complete: function(){
                $scope.$apply(function() {
                    $scope.playSound(8);
					$scope.startWordsStep15();
				});
            }
        };


        $scope.heli2 = $("#heli2");
        $scope.heli2.animateSprite(settings);

        $scope.heli2.animateSprite('frame', 0);

        setTimeout(function() {
            $scope.heli2.animateSprite('restart', 'run');
        }, 1000);
    }

    $scope.startWordsStep15 = function() {
        // todo
        var dur = 4000;
        var word5 = function() {
            $scope.stroka5S15.animate({"left": "0%"}, {duration: dur, queue:false, complete: function() {
                //
            }});
        }
        var word4 = function() {
            $scope.stroka4S15.animate({"left": "0%"}, {duration: dur, queue:false, complete: function() {
                word5();
            }});
        }
        var word3 = function() {
            $scope.stroka3S15.animate({"left": "0%"}, {duration: dur, queue:false, complete: function() {
                word4();
            }});
        }
        var word2 = function() {
            $scope.stroka2S15.animate({"left": "0%"}, {duration: dur, queue:false, complete: function() {
                word3();
            }});
        }
        var word1 = function() {
            $scope.stroka1S15.animate({"left": "0%"}, {duration: dur, queue:false, complete: function() {
                word2();
            }});
        }

        word1();
    }

    $scope.checkIfAllFilledS4 = function() {
        var initialValues = ['x','y','z','v'];

        if ($scope.tableTextColumn1[0] != '' && $scope.tableTextColumn1[0] != initialValues[0] &&
            $scope.tableTextColumn1[1] != '' && $scope.tableTextColumn1[1] != initialValues[1] &&
            $scope.tableTextColumn1[2] != '' && $scope.tableTextColumn1[2] != initialValues[2] &&
            $scope.tableTextColumn1[3] != '' && $scope.tableTextColumn1[3] != initialValues[3]) {
            $scope.showCheckButtonS4 = true;
        }
    }
    $scope.checkIfAllFilledS12 = function() {
        var initialValues = ['x','y','z','v'];

        if ($scope.answerS12.trim() != '') {
            $scope.showCheckButtonS12 = true;
        }
    }
    $scope.checkIfAllFilledS7 = function() {
        if ($scope.answerTextS7[0].trim() != '' &&
            $scope.answerTextS7[1].trim() != '' &&
            $scope.answerTextS7[2].trim() != '' &&
            $scope.answerTextS7[3].trim() != '') {
            $scope.showCheckButtonS7 = true;
        }
    }

    $scope.answer11FS14 = null;
    $scope.answer12FS14 = null;
    $scope.answer21FS14 = null;
    $scope.answer22FS14 = null;
    $scope.answer31FS14 = null;
    $scope.answer32FS14 = null;
    $scope.answer41FS14 = null;
    $scope.answer42FS14 = null;

    $scope.kolchedanShowS16 = false;
    $scope.moleculeS16 = $("#molecule_s16");
    $scope.showMoleculeS16 = false;
    $scope.showMolecule2S16 = false;

    $scope.molecule2Status = 1;
    $scope.showTableS16 = false;
    $scope.showTableElemS16 = false;

    $scope.blinkTableElementsS16 = function() {
        var limit = 6;
        var speed = 200;
        var count = 0;
        var highlight = function() {
            if (count == limit) {
                return;
            }
            $scope.$apply(function() {
                $scope.showTableElemS16 = !$scope.showTableElemS16;
            });
            ++count;

            setTimeout(highlight, speed);
        }
        setTimeout(highlight, speed);
    }
    $scope.startStep16 = function() {
        // todo
        $scope.stepNum = 16;
        $scope.playSound(9, function() {

        });

        setTimeout(function() {
            $scope.$apply(function() {
                $scope.kolchedanShowS16 = true;
            });
        }, 4000);

        setTimeout(function() {
            $scope.$apply(function () {
                $scope.showTableS16 = true;
                $scope.blinkTableElementsS16();
            });
        }, 7000);

        setTimeout(function() {
            $scope.$apply(function() {
                $scope.showMoleculeS16 = true;
                $scope.moleculeS16.animate({"left": "25.25%", top: "20%"}, {duration: 5000, queue:false, complete: function() {
                    $scope.$apply(function() {
                        $scope.showMolecule2S16 = true;
                        $scope.showMoleculeS16 = false;
                    });
                    setTimeout(function() {
                        $scope.$apply(function() {
                            $scope.molecule2Status = 1;
                        })
                    }, 2500);
                    setTimeout(function() {
                        $scope.$apply(function() {
                            $scope.molecule2Status++;
                        })
                    }, 2000);
                    setTimeout(function() {
                        $scope.$apply(function() {
                            $scope.molecule2Status++;
                        })
                    }, 4000);
                    setTimeout(function() {
                        $scope.$apply(function() {
                            $scope.molecule2Status++;
                        })
                    }, 6000);
                }});
            });
        }, 17000);
    };

    $scope.correctAnswers1S17 = '3';
    $scope.correctAnswers2S17 = '23';
    $scope.correctAnswers3S17 = ['23', '1', '12', '48', '84'];
    $scope.checkButtonPressedS17 = false;
    $scope.showCheckButtonS17 = false;
    $scope.showFontBoldS17 = false;

    $scope.areAnswersCorrectS17 = [false, false, false, false, false, false, false];
    $scope.changeToCorrectAnswersS17 = function() {
        $scope.areAnswersCorrectS17[0] = $scope.answer1S17 === $scope.correctAnswers1S17;
        $scope.areAnswersCorrectS17[1] = $scope.answer2S17 === $scope.correctAnswers2S17;

        $scope.areAnswersCorrectS17[2] = $scope.answer31S17 === $scope.correctAnswers3S17[0];
        $scope.areAnswersCorrectS17[3] = $scope.answer32S17 === $scope.correctAnswers3S17[1];
        $scope.areAnswersCorrectS17[4] = $scope.answer33S17 === $scope.correctAnswers3S17[2];
        $scope.areAnswersCorrectS17[5] = $scope.answer34S17 === $scope.correctAnswers3S17[3];
        $scope.areAnswersCorrectS17[6] = $scope.answer35S17 === $scope.correctAnswers3S17[4];

        $scope.input1_s17.val($scope.correctAnswers1S17);
        $scope.input2_s17.val($scope.correctAnswers2S17);

        $scope.input31_s17.val($scope.correctAnswers3S17[0]);
        $scope.input32_s17.val($scope.correctAnswers3S17[1]);
        $scope.input33_s17.val($scope.correctAnswers3S17[2]);
        $scope.input34_s17.val($scope.correctAnswers3S17[3]);
        $scope.input35_s17.val($scope.correctAnswers3S17[4]);

        $scope.showFontBoldS17 = true;
    }

    $scope.bgColorCheckS17 = function(num) {

        if (!$scope.checkButtonPressedS17) {
            return '';
        }

        if ($scope.showFontBoldS17) {
            if (num == 1) {
                if ($scope.answer1S17 !== $scope.correctAnswers1S17) {
                    return 'background_color_green bold_font';
                }
            } else if (num == 2) {
                if ($scope.answer2S17 !== $scope.correctAnswers2S17) {
                    return 'background_color_green bold_font';
                }
            } else if (num == 31) {
                if ($scope.answer31S17 !== $scope.correctAnswers3S17[0]) {
                    return 'background_color_green bold_font';
                }
            } else if (num == 32) {
                if ($scope.answer32S17 !== $scope.correctAnswers3S17[1]) {
                    return 'background_color_green bold_font';
                }
            } else if (num == 33) {
                if ($scope.answer33S17 !== $scope.correctAnswers3S17[2]) {
                    return 'background_color_green bold_font';
                }
            } else if (num == 34) {

                if ($scope.answer34S17 !== $scope.correctAnswers3S17[3]) {
                    return 'background_color_green bold_font';
                }
            }  else if (num == 35) {
                if ($scope.answer35S17 !== $scope.correctAnswers3S17[4]) {
                    return 'background_color_green bold_font';
                }
            }
        }



        if (num == 1) {
            if ($scope.answer1S17 === $scope.correctAnswers1S17) {
                return 'background_color_green';
            } else {
                return 'background_color_red';
            }
        } else if (num == 2) {
            if ($scope.answer2S17 === $scope.correctAnswers2S17) {
                return 'background_color_green';
            } else {
                return 'background_color_red';
            }
        } else if (num == 31) {
            if ($scope.answer31S17 === $scope.correctAnswers3S17[0]) {
                return 'background_color_green';
            } else {
                return 'background_color_red';
            }
        } else if (num == 32) {
            if ($scope.answer32S17 === $scope.correctAnswers3S17[1]) {
                return 'background_color_green';
            } else {
                return 'background_color_red';
            }
        } else if (num == 33) {

            if ($scope.answer33S17 === $scope.correctAnswers3S17[2]) {
                return 'background_color_green';
            }  else {
                return 'background_color_red';
            }
        } else if (num == 34) {

            if ($scope.answer34S17 === $scope.correctAnswers3S17[3]) {
                return 'background_color_green';
            } else {
                return 'background_color_red';
            }
        } else if (num == 35) {

            if ($scope.answer35S17 === $scope.correctAnswers3S17[4]) {
                return 'background_color_green';
            } else {
                return 'background_color_red';
            }
        }
    }

    $scope.startStep17 = function() {
        // todo
        $scope.stepNum = 17;

    }

    $scope.checkZeroS14 = function(num) {
        if (num == 1) {
            return !($scope.answer11FS14.text() === "0" || $scope.answer11FS14.text() === "1");
        } else if (num == 2) {
            return !($scope.answer12FS14.text() === "0" || $scope.answer12FS14.text() === "1");
        } else if (num == 3) {
            return !($scope.answer21FS14.text() === "0" || $scope.answer21FS14.text() === "1");
        } else if (num == 4) {
            return !($scope.answer22FS14.text() === "0" || $scope.answer22FS14.text() === "1");
        } else if (num == 5) {
            return !($scope.answer31FS14.text() === "0" || $scope.answer31FS14.text() === "1");
        } else if (num == 6) {
            return !($scope.answer32FS14.text() === "0" || $scope.answer32FS14.text() === "1");
        } else if (num == 7) {
            return !($scope.answer41FS14.text() === "0" || $scope.answer41FS14.text() === "1");
        } else if (num == 8) {
            return !($scope.answer42FS14.text() === "0" || $scope.answer42FS14.text() === "1");
        }
    }
    $scope.input1_s17 = $("#input1_s17");
    $scope.input2_s17 = $("#input2_s17");

    $scope.input31_s17 = $("#input31_s17");
    $scope.input32_s17 = $("#input32_s17");
    $scope.input33_s17 = $("#input33_s17");
    $scope.input34_s17 = $("#input34_s17");
    $scope.input35_s17 = $("#input35_s17");

    $scope.answer1S17 = '';
    $scope.answer2S17 = '';
    $scope.answer31S17 = '';
    $scope.answer32S17 = '';
    $scope.answer33S17 = '';
    $scope.answer34S17 = '';
    $scope.answer35S17 = '';

    $scope.showInstructionPole = true;

    $scope.tur1Letters = [$("#tur1letter1"), $("#tur1letter2"), $("#tur1letter3"), $("#tur1letter4"), $("#tur1letter5"),
                          $("#tur1letter6"), $("#tur1letter7"), $("#tur1letter8"), $("#tur1letter9"), $("#tur1letter10"),
                         $("#tur1letter11"), $("#tur1letter12"), $("#tur1letter13"), $("#tur1letter14"),
                        $("#tur1letter15"), $("#tur1letter16")];
    
    $scope.tur2Letters = [$("#tur2letter1"), $("#tur2letter2"), $("#tur2letter3"), $("#tur2letter4"), $("#tur2letter5"),
        $("#tur2letter6"), $("#tur2letter7"), $("#tur2letter8"), $("#tur2letter9"), $("#tur2letter10")
        , $("#tur2letter11"), $("#tur2letter12"), $("#tur2letter13")];

    $scope.tur3Letters = [$("#tur3letter1"),$("#tur3letter2"),$("#tur3letter3"),$("#tur3letter4"),$("#tur3letter5")];
    $scope.tur4Letters = [$("#tur4letter1"),$("#tur4letter2"),$("#tur4letter3")];

    $scope.tur1Values = ['Г', 'И', 'Д', 'Р', 'О', 'К', 'С', 'И', 'Д', 'К', 'А', 'Л', 'Ь', 'Ц', 'И', 'Я'];
    $scope.showCorrectLetterT1 = [false, false, false, false, false, false, false, false, false, false, false, false,
                                    false, false, false, false];
    $scope.letterDoneTur1 = [false, false, false, false, false, false, false, false, false, false, false, false, false,
                                false, false, false];

    $scope.tur2Values = ['С', 'Е', 'Р', 'Н', 'А', 'Я', 'К', 'И', 'С', 'Л', 'О', 'Т', 'А'];
    $scope.showCorrectLetterT2 = [false, false, false, false, false, false, false, false, false, false, false, false, false, false];
    $scope.letterDoneTur2 = [false, false, false, false, false, false, false, false, false, false, false, false, false, false];
    
    $scope.tur3Values = ['Р', 'Т', 'У', 'Т', 'Ь'];
    $scope.showCorrectLetterT3 = [false, false, false, false, false];
    $scope.letterDoneTur3 = [false, false, false, false, false];

    $scope.tur4Values = ['4', '1', '8'];
    $scope.showCorrectLetterT4 = [false, false, false];
    $scope.letterDoneTur4 = [false, false, false];
    
	$scope.cardsT1 = [
		$("#tur1cardsletter1"), $("#tur1cardsletter2"), $("#tur1cardsletter3"),
        $("#tur1cardsletter4"), $("#tur1cardsletter5"), $("#tur1cardsletter6"),
        $("#tur1cardsletter7"), $("#tur1cardsletter8"), $("#tur1cardsletter9"),
        $("#tur1cardsletter10"), $("#tur1cardsletter11"), $("#tur1cardsletter12"),
        $("#tur1cardsletter13"), $("#tur1cardsletter14"), $("#tur1cardsletter15"), $("#tur1cardsletter16")
	];


    $scope.cardsT2 = [$("#tur2cardsletter1"), $("#tur2cardsletter2"), $("#tur2cardsletter3"),
        $("#tur2cardsletter4"), $("#tur2cardsletter5"), $("#tur2cardsletter6"),
        $("#tur2cardsletter7"), $("#tur2cardsletter8"), $("#tur2cardsletter9")
        , $("#tur2cardsletter10"), $("#tur2cardsletter11"), $("#tur2cardsletter12"), $("#tur2cardsletter13")];

    $scope.cardsT3 = [$("#tur3cardsletter1"), $("#tur3cardsletter2"), $("#tur3cardsletter3"),
                      $("#tur3cardsletter4"), $("#tur3cardsletter5")];

    $scope.cardsT4 = [$("#tur4cardsletter1"), $("#tur4cardsletter2"), $("#tur4cardsletter3")];
					  
    $scope.showingLetterNum1 = 0;
    $scope.openLetterTur1 = function(num) {
        // todo
        $scope.showingLetterNum1 = num - 1;
		console.log("cards t1 ", $scope.cardsT1[num - 1]);
        $scope.cardsT1[num - 1].animateSprite('play', 'run');
//        $scope.barabanAllowed = true;
        if ($scope.showingLetterNum1 == 15) {
            $scope.showTick = true;
            $scope.turDone1 = true;
        }

        if ($scope.showingLetterNum1 == 15) {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.marksPole += $scope.currentPointsToWin;
                    $scope.barabanAllowed = true;
                    $scope.showInstructionPole = true;
                });
            }, 2000);
        }
    }

    $scope.showingLetterNum2 = 0;
    $scope.openLetterTur2 = function(num) {
        // todo
        $scope.showingLetterNum2 = num - 1;
        $scope.cardsT2[num - 1].animateSprite('play', 'run');
//        $scope.barabanAllowed = true;

        if ($scope.showingLetterNum2 == 12) {
            $scope.showTick = true;
            $scope.turDone2 = true;
        }

        if ($scope.showingLetterNum2 == 12) {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.marksPole += $scope.currentPointsToWin;
                    $scope.showInstructionPole = true;
                    $scope.barabanAllowed = true;
                });
            }, 2000);
        }
    }

    $scope.showingLetterNum3 = 0;
    $scope.openLetterTur3 = function(num) {
        // todo
        $scope.showingLetterNum3 = num - 1;
        $scope.cardsT3[num - 1].animateSprite('play', 'run');
//        $scope.barabanAllowed = true;

        if ($scope.showingLetterNum3 == 4) {
            $scope.showTick = true;
            $scope.turDone3 = true;
        }

        if ($scope.showingLetterNum3 == 4) {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.marksPole += $scope.currentPointsToWin;
                    $scope.showInstructionPole = true;
                    $scope.barabanAllowed = true;
                });
            }, 2000);
        }
    }

    $scope.sprite1S13 = $("#sprite1_S13");
    $scope.sprite2S13 = $("#sprite2_S13");
    $scope.sprite3S13 = $("#sprite3_S13");
    $scope.sprite4S13 = $("#sprite4_S13");

    $scope.showingLetterNum4 = 0;
    $scope.openLetterTur4 = function(num) {
        // todo
        console.log("open letter 4")
        $scope.showingLetterNum4 = num - 1;
        $scope.cardsT4[num - 1].animateSprite('play', 'run');
//        $scope.barabanAllowed = true;

        if ($scope.showingLetterNum4 == 2) {
            $scope.showTick = true;
            $scope.turDone4 = true;
            $scope.showCheckButtonPole = true;
        }

        if ($scope.showingLetterNum4 == 2) {
            setTimeout(function() {
                $scope.$apply(function(){
                    $scope.marksPole += $scope.currentPointsToWin;
                    $scope.showInstructionPole = true;
                    $scope.barabanAllowed = true;
                });
            }, 2000);
        }
    }

    $scope.errorsStatus = [0, 0, 0, 0];
    $scope.allowedCharacter = function(value, letter, ind) {
        if ($scope.turNumber != 4) {
            console.log("character tur != 4");
            if (value.length > 1) {
                letter[ind].val(value[0]);
                return false;
            }

            var valUp = value.toUpperCase();
            if (valUp <= 'Я' && valUp >= 'А') {
//                return true;
            } else {
                console.log("compare valUp: ", valUp, valUp <= 'Я' && valUp >= 'А')
                return false;
            }
        } else {
            if (value <= '9' && value >= '0') {
//                return true;
            } else {
                return false;
            }
        }

        value = value.toUpperCase();
        if ($scope.turNumber == 1) {
            if (value != $scope.tur1Values[ind]) {
                if ($scope.errorsStatus[0] + 1 == 3) {
                    $scope.showPodskazka();
                    $scope.errorsStatus[0]++;
                }
                $scope.errorsStatus[0]++;
                return false;
            }
        } else if ($scope.turNumber == 2) {
            if (value != $scope.tur2Values[ind]) {
                if ($scope.errorsStatus[1] + 1 == 3) {
                    $scope.showPodskazka();
                    $scope.errorsStatus[1]++;
                }
                $scope.errorsStatus[1]++;
                return false;
            }
        } else if ($scope.turNumber == 3) {
            if (value != $scope.tur3Values[ind]) {
                if ($scope.errorsStatus[2] + 1 == 3) {
                    $scope.showPodskazka();
                    $scope.errorsStatus[2]++;
                }
                $scope.errorsStatus[2]++;
                return false;
            }
        } else if ($scope.turNumber == 4) {
            if (value != $scope.tur4Values[ind]) {
                if ($scope.errorsStatus[3 ] + 1 == 3) {
                    $scope.showPodskazka();
                    $scope.errorsStatus[3]++;
                }
                $scope.errorsStatus[3]++;
                return false;
            }
        }
        return true;
    }
    $scope.showPodskazka = function() {
        if ($scope.turNumber == 1) {
            if (!$scope.letterDoneTur1[0]) {
                $scope.letterDoneTur1[0] = true;
                $scope.openLetterTur1(1);
            }
            if (!$scope.letterDoneTur1[2]) {
                $scope.letterDoneTur1[2] = true;
                $scope.openLetterTur1(3);
            }
            if (!$scope.letterDoneTur1[4]) {
                $scope.letterDoneTur1[4] = true;
                $scope.openLetterTur1(5);
            }
            if (!$scope.letterDoneTur1[9]) {
                $scope.letterDoneTur1[9] = true;
                $scope.openLetterTur1(10);
            }
            if (!$scope.letterDoneTur1[13]) {
                $scope.letterDoneTur1[13] = true;
                $scope.openLetterTur1(14);
            }
        } else if ($scope.turNumber == 2) {
            if (!$scope.letterDoneTur2[0]) {
                $scope.letterDoneTur2[0] = true;
                $scope.openLetterTur2(1);
            }
            if (!$scope.letterDoneTur2[3]) {
                $scope.letterDoneTur2[3] = true;
                $scope.openLetterTur2(4);
            }
            if (!$scope.letterDoneTur2[6]) {
                $scope.letterDoneTur2[6] = true;
                $scope.openLetterTur2(7);
            }
            if (!$scope.letterDoneTur2[8]) {
                $scope.letterDoneTur2[8] = true;
                $scope.openLetterTur2(9);
            }
        } else if ($scope.turNumber == 3) {
            if (!$scope.letterDoneTur3[4]) {
                $scope.letterDoneTur3[4] = true;
                $scope.openLetterTur3(5);
            }
        }
    }
    $scope.poleLettersInit = function() {
        $scope.tur4Letters[0].bind("input", function() {
            var ind = 0;
            $scope.$apply(function() {
                var value = $scope.tur4Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur4Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur4Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur4Values[ind]) {
                    $scope.letterDoneTur4[ind] = true;
                    $scope.openLetterTur4(ind + 1);
                }
            });
        });
        $scope.tur4Letters[1].bind("input", function() {
            var ind = 1;
            $scope.$apply(function() {
                var value = $scope.tur4Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur4Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur4Letters, ind);
                    return;
                }
                if (value == $scope.tur4Values[ind]) {
                    $scope.letterDoneTur4[ind] = true;
                    $scope.openLetterTur4(ind + 1);
                }
            });
        });
        $scope.tur4Letters[2].bind("input", function() {
            var ind = 2;
            $scope.$apply(function() {
                var value = $scope.tur4Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur4Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur4Letters, ind);
                    return;
                }
                if (value == $scope.tur4Values[ind]) {
                    $scope.letterDoneTur4[ind] = true;
                    $scope.openLetterTur4(ind + 1);
                }
            });
        });

//        ============================================
        
        $scope.tur1Letters[0].bind("input", function() {
            var ind = 0;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {

                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[1].bind("input", function() {
            var ind = 1;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[2].bind("input", function() {
            var ind = 2;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[3].bind("input", function() {
            var ind = 3;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[4].bind("input", function() {
            var ind = 4;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[5].bind("input", function() {
            var ind = 5;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[6].bind("input", function() {
            var ind = 6;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[7].bind("input", function() {
            var ind = 7;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[8].bind("input", function() {
            var ind = 8;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

        $scope.tur1Letters[9].bind("input", function() {
            var ind = 9;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

        $scope.tur1Letters[10].bind("input", function() {
            var ind = 10;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

        $scope.tur1Letters[11].bind("input", function() {
            var ind = 11;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

        $scope.tur1Letters[12].bind("input", function() {
            var ind = 12;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

        $scope.tur1Letters[13].bind("input", function() {
            var ind = 13;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

        $scope.tur1Letters[14].bind("input", function() {
            var ind = 14;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });
        $scope.tur1Letters[15].bind("input", function() {
            var ind = 15;
            $scope.$apply(function() {
                var value = $scope.tur1Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur1Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur1Letters, ind);
                    return;
                }
                if (value == $scope.tur1Values[ind]) {
                    $scope.letterDoneTur1[ind] = true;
                    $scope.openLetterTur1(ind + 1);
                }
            });
        });

//        tur 2
        $scope.tur2Letters[0].bind("input", function() {
            var ind = 0;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[1].bind("input", function() {
            var ind = 1;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[2].bind("input", function() {
            var ind = 2;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[3].bind("input", function() {
            var ind = 3;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[4].bind("input", function() {
            var ind = 4;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[5].bind("input", function() {
            var ind = 5;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[6].bind("input", function() {
            var ind = 6;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[7].bind("input", function() {
            var ind = 7;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[8].bind("input", function() {
            var ind = 8;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[9].bind("input", function() {
            var ind = 9;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[10].bind("input", function() {
            var ind = 10;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[11].bind("input", function() {
            var ind = 11;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });

        $scope.tur2Letters[12].bind("input", function() {
            var ind = 12;
            $scope.$apply(function() {
                var value = $scope.tur2Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur2Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur2Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur2Values[ind]) {
                    $scope.letterDoneTur2[ind] = true;
                    $scope.openLetterTur2(ind + 1);
                }
            });
        });
		
		//tur 3
		$scope.tur3Letters[0].bind("input", function() {
            var ind = 0;
            $scope.$apply(function() {
                var value = $scope.tur3Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur3Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur3Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur3Values[ind]) {
                    $scope.letterDoneTur3[ind] = true;
                    $scope.openLetterTur3(ind + 1);
                }
            });
        });
		$scope.tur3Letters[1].bind("input", function() {
            var ind = 1;
            $scope.$apply(function() {
                var value = $scope.tur3Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur3Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur3Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur3Values[ind]) {
                    $scope.letterDoneTur3[ind] = true;
                    $scope.openLetterTur3(ind + 1);
                }
            });
        });
		$scope.tur3Letters[2].bind("input", function() {
            var ind = 2;
            $scope.$apply(function() {
                var value = $scope.tur3Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur3Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur3Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur3Values[ind]) {
                    $scope.letterDoneTur3[ind] = true;
                    $scope.openLetterTur3(ind + 1);
                }
            });
        });
		$scope.tur3Letters[3].bind("input", function() {
            var ind = 3;
            $scope.$apply(function() {
                var value = $scope.tur3Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur3Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur3Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur3Values[ind]) {
                    $scope.letterDoneTur3[ind] = true;
                    $scope.openLetterTur3(ind + 1);
                }
            });
        });
		$scope.tur3Letters[4].bind("input", function() {
            var ind = 4;
            $scope.$apply(function() {
                var value = $scope.tur3Letters[ind].val().trim().toUpperCase();
                if (!$scope.allowedCharacter(value, $scope.tur3Letters, ind)) {
                    $scope.clearPoleLetter($scope.tur3Letters, ind);
                    return;
                }
                console.log(value)
                if (value == $scope.tur3Values[ind]) {
                    $scope.letterDoneTur3[ind] = true;
                    $scope.openLetterTur3(ind + 1);
                }
            });
        });
    }

    $scope.playIncorrectSound = function() {
        var intro = new Audio('sounds/incorrect.mp3');
        intro.load();
        intro.play();
    }
    $scope.clearPoleLetter = function(letter, ind) {
        $scope.playIncorrectSound();
        setTimeout(function() {
            letter[ind].val("");
        }, 500);
   }
    $scope.wheel = $("#wheel");
    $scope.wheelAnimSettings = null;
    $scope.cardsAnimSettings = null;

    $scope.turDone1 = false;
    $scope.turDone2 = false;
    $scope.turDone3 = false;
    $scope.turDone4 = false;

    $scope.showF2S13 = true;
    $scope.showF3S13 = true;

    $scope.initEverything = function() {
        $scope.sprite4AnimSettings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 6,
            rows: 2,
            animations: {

                run: [0, 1, 2, 3, 4,5]
            },
            complete: function(){
                $scope.$apply(function() {
                    $scope.showF2S13 = true;
                });
            }
        };
        $scope.sprite4S13.animateSprite($scope.sprite4AnimSettings);

        $scope.sprite3AnimSettings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 8,
            rows: 2,
            animations: {
                run: [0, 1, 2, 3, 4,5,6,7]
            },
            complete: function(){
                $scope.$apply(function() {
                    $scope.sprite4S13Show = true;
                    $scope.sprite4S13.animateSprite('frame', 0);
                    $scope.sprite4S13.animateSprite('restart', 'run');
                    $scope.showF3S13 = true;
                });
            }
        };
        $scope.sprite3S13.animateSprite($scope.sprite3AnimSettings);

        $scope.sprite2AnimSettings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 10,
            rows: 2,
            animations: {
                run: [0, 1, 2, 3, 4,5,6,7,8,9]
            },
            complete: function(){
                $scope.$apply(function() {
                    $scope.sprite3S13Show = true;
                    $scope.sprite3S13.animateSprite('frame', 0);
                    $scope.sprite3S13.animateSprite('restart', 'run');
                });
            }
        };
        $scope.sprite2S13.animateSprite($scope.sprite2AnimSettings);

        $scope.sprite1AnimSettings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 12,
            rows: 2,
            animations: {
                run: [0, 1, 2, 3, 4,5,6,7,8,9,10,11]
            },
            complete: function(){
                $scope.$apply(function() {
                    $scope.sprite2S13Show = true;
                    $scope.sprite2S13.animateSprite('frame', 0);
                    $scope.sprite2S13.animateSprite('restart', 'run');
                    $scope.showAlS13 = true;
                });
            }
        };
        $scope.sprite1S13.animateSprite($scope.sprite1AnimSettings);
        $scope.sprite1S13.animateSprite('play', 'run');
        $scope.cardsAnimSettings = {
            fps: 5,
            loop: false,
            autoplay: false,
            columns: 5,
            rows: 2,
            animations: {
                run: [0, 1, 2, 3, 4]
            },
            complete: function(){
                $scope.$apply(function() {
                    $scope.showCorrectLetterT1[$scope.showingLetterNum1] = true;
//                    console.log("setting letter num to true: " + $scope.showingLetterNum1);
                });
            }
        };

        for (var i = 0 ; i < $scope.cardsT1.length; ++i) {
            $scope.cardsT1[i].animateSprite($scope.cardsAnimSettings);
        }
        for (var i = 0 ; i < $scope.cardsT2.length; ++i) {
            $scope.cardsT2[i].animateSprite($scope.cardsAnimSettings);
        }
        for (var i = 0 ; i < $scope.cardsT3.length; ++i) {
            $scope.cardsT3[i].animateSprite($scope.cardsAnimSettings);
        }
        for (var i = 0 ; i < $scope.cardsT4.length; ++i) {
            $scope.cardsT4[i].animateSprite($scope.cardsAnimSettings);
        }
        $scope.wheelAnimSettings = {
            fps: 5,
            loop: true,
            autoplay: false,
            columns: 3,
            rows: 2,
            animations: {
                run: [0, 1, 2]
            },
            complete: function(){
                $scope.$apply(function() {

                })
            }
        };


        $scope.wheel.animateSprite($scope.wheelAnimSettings);
        $scope.answer11FS14 = $("#formula_number_11_s14");
        $scope.answer12FS14 = $("#formula_number_12_s14");
        $scope.answer21FS14 = $("#formula_number_21_s14");
        $scope.answer22FS14 = $("#formula_number_22_s14");

        $scope.answer31FS14 = $("#formula_number_31_s14");
        $scope.answer32FS14 = $("#formula_number_32_s14");
        $scope.answer41FS14 = $("#formula_number_41_s14");
        $scope.answer42FS14 = $("#formula_number_42_s14");


        $scope.poleLettersInit();
        $scope.input1_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer1S17 = $scope.input1_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });
        $scope.input2_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer2S17 = $scope.input2_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });

        $scope.input31_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer31S17 = $scope.input31_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });
        $scope.input32_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer32S17 = $scope.input32_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });
        $scope.input33_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer33S17 = $scope.input33_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });
        $scope.input34_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer34S17 = $scope.input34_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });
        $scope.input35_s17.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer35S17 = $scope.input35_s17.val();
                $scope.checkIfAllFilledS17();
            });
        });


        $scope.input11S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer11S14 = $scope.input11S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input12S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer12S14 = $scope.input12S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input21S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer21S14 = $scope.input21S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input22S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer22S14 = $scope.input22S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input31S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer31S14 = $scope.input31S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input32S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer32S14 = $scope.input32S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input41S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer41S14 = $scope.input41S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });
        $scope.input42S14.bind("input", function() {
            $scope.$apply(function() {
                $scope.answer42S14 = $scope.input42S14.val().toUpperCase();
                $scope.checkIfAllFilledS14();
            });
        });

        $scope.showCheckButtonS14 = false;
        $scope.checkButtonClickedS14 = function() {
            // todo
            $scope.checkButtonPressedS14 = true;

            $scope.answerIsCorrectFS14[0] = $scope.answer11FS14.text() === '0';
            $scope.answerIsCorrectFS14[1] = $scope.answer12FS14.text().trim() === '2';
            $scope.answerIsCorrectFS14[2] = $scope.answer21FS14.text().trim() === '0';
            $scope.answerIsCorrectFS14[3] = $scope.answer22FS14.text().trim() === '2';
            $scope.answerIsCorrectFS14[4] = $scope.answer31FS14.text().trim() === '0';
            $scope.answerIsCorrectFS14[5] = $scope.answer32FS14.text().trim() === '3';
            $scope.answerIsCorrectFS14[6] = $scope.answer41FS14.text().trim() === '0';
            $scope.answerIsCorrectFS14[7] = $scope.answer41FS14.text().trim() === '0';

            $scope.answerIsCorrectS14[0] = $scope.answer11S14 === $scope.rightAnswersS14[0];
            $scope.answerIsCorrectS14[1] = $scope.answer12S14 === $scope.rightAnswersS14[1];
            $scope.answerIsCorrectS14[2] = $scope.answer21S14 === $scope.rightAnswersS14[2];
            $scope.answerIsCorrectS14[3] = $scope.answer22S14 === $scope.rightAnswersS14[3];
            $scope.answerIsCorrectS14[4] = $scope.answer31S14 === $scope.rightAnswersS14[4];
            $scope.answerIsCorrectS14[5] = $scope.answer32S14 === $scope.rightAnswersS14[5];
            $scope.answerIsCorrectS14[6] = $scope.answer41S14 === $scope.rightAnswersS14[6];
            $scope.answerIsCorrectS14[7] = $scope.answer42S14 === $scope.rightAnswersS14[7];

            setTimeout(function() {
                $scope.$apply(function() {
                    $scope.changeToCorrectAnswersS14();
                    if ($scope.answerIsCorrectS14[0] && $scope.answerIsCorrectS14[1] &&
                        $scope.answerIsCorrectS14[2] && $scope.answerIsCorrectS14[3] &&
                        $scope.answerIsCorrectS14[4] && $scope.answerIsCorrectS14[5] &&
                        $scope.answerIsCorrectS14[6] && $scope.answerIsCorrectS14[7] &&
                        $scope.answerIsCorrectFS14[0] && $scope.answerIsCorrectFS14[1] &&
                        $scope.answerIsCorrectFS14[2] && $scope.answerIsCorrectFS14[3] &&
                        $scope.answerIsCorrectFS14[4] && $scope.answerIsCorrectFS14[5] &&
                        $scope.answerIsCorrectFS14[6] && $scope.answerIsCorrectFS14[7]) {
                        $scope.giveCoin(function() {
//                            $scope.startStep15();
                        });
                    } else {
                        setTimeout(function() {
                            $scope.$apply(function() {
//                                $scope.startStep15();
                            });
                        }, 5000);
                    }
                });
            }, 3000);
        }

        $scope.answer11S14 = '';
        $scope.answer12S14 = '';
        $scope.answer21S14 = '';
        $scope.answer22S14 = '';
        $scope.answer31S14 = '';
        $scope.answer32S14 = '';
        $scope.answer41S14 = '';
        $scope.answer42S14 = '';

        $scope.checkIfAllFilledS14 = function() {
            $scope.showCheckButtonS14 =
                $scope.answer11S14 != '' &&
                    $scope.answer12S14 != '' &&
                    $scope.answer21S14 != '' &&
                    $scope.answer22S14 != '' &&
                    $scope.answer31S14 != '' &&
                    $scope.answer32S14 != '' &&
                    $scope.answer41S14 != '' &&
                    $scope.answer42S14 != '';
        }

        $scope.showCheckButtonS17 = false;
        $scope.checkIfAllFilledS17 = function() {
            $scope.showCheckButtonS17 =
                $scope.answer1S17 != '' &&
                    $scope.answer2S17 != '' &&
                    $scope.answer31S17 != '' &&
                    $scope.answer32S17 != '' &&
                    $scope.answer33S17 != '' &&
                    $scope.answer34S17 != '' &&
                    $scope.answer35S17 != '';
        }

        $scope.inputS12.bind("input", function() {
            $scope.$apply(function() {
                $scope.answerS12 = $scope.inputS12.val().replace(",", ".");
                $scope.checkIfAllFilledS12();
            });
        });

        $scope.input1S7.bind("input", function() {
            $scope.$apply(function() {
                $scope.answerTextS7[0] = $scope.input1S7.val().replace(",", ".");
                $scope.checkIfAllFilledS7();
            });
        });

        $scope.input2S7.bind("input", function() {
            $scope.$apply(function() {
                $scope.answerTextS7[1] = $scope.input2S7.val().replace(",", ".");
                $scope.checkIfAllFilledS7();
            });
        });

        $scope.input3S7.bind("input", function() {
            $scope.$apply(function() {
                $scope.answerTextS7[2] = $scope.input3S7.val().replace(",", ".");
                $scope.checkIfAllFilledS7();
            });
        });

        $scope.input4S7.bind("input", function() {
            $scope.$apply(function() {
                $scope.answerTextS7[3] = $scope.input4S7.val().replace(",", ".");
                $scope.checkIfAllFilledS7();
            });
        });

        $scope.input1S4.bind("input", function() {
            $scope.$apply(function() {
                $scope.tableTextColumn1[0] = $scope.input1S4.val();
                $scope.checkIfAllFilledS4();
            });
        });

        $scope.input2S4.bind("input", function() {
            $scope.$apply(function() {
                $scope.tableTextColumn1[1] = $scope.input2S4.val();
                $scope.checkIfAllFilledS4();
            });
        });

        $scope.input3S4.bind("input", function() {
            $scope.$apply(function() {
                $scope.tableTextColumn1[2] = $scope.input3S4.val();
                $scope.checkIfAllFilledS4();
            });
        });

        $scope.input4S4.bind("input", function() {
            $scope.$apply(function() {
                $scope.tableTextColumn1[3] = $scope.input4S4.val();
                $scope.checkIfAllFilledS4();
            });
        });
        /*
         $scope.treeInButtonDrag = new Draggabilly("#tree_in_button", {
         containment: "#main"
         });

         // cat drag
         $scope.catInButtonDrag.on("dragEnd", function(instance, event, pointer) {
         $scope.$apply(function() {
         $scope.dragEnd2(instance, event, pointer, 'cat');
         });
         });

         $scope.orangeLadderDrag.on("dragStart", function(instance, event, pointer) {
         $scope.$apply(function() {
         $scope.dragStart1(instance, event, pointer, 'orange_ladder');
         });
         });

         $scope.skameikaAnim1= {
         fps: 6,
         loop: false,
         autoplay: false,
         columns: 3,
         rows: 2,
         animations: {
         run: $scope.returnArr(0, 2)
         },
         complete: function(){
         setTimeout(function() {
         $scope.$apply(function() {
         $scope.anim1Stage++;
         $scope.skameikaAnimation1.animateSprite('init', 'run');
         $scope.skameikaAnimation2.animateSprite('restart', 'run');
         });
         }, 1500);
         }
         };
         */
    };

    $scope.returnArr = function(start,end){
        var a,b;
        var tempArr = [], counter=0;
        if(start > end){
            for(var i=start; i>=end; i--){
                tempArr.push(i);
            }
        }
        else {
            for(var i=start; i<=end; i++){
                tempArr.push(i);
            }
        }
        return tempArr;
    };

}]);


$(document).ready(function() {
    //loadMenu();
    var game = angular.element($('body')).scope();

    $(".inputs4,.inputs17").keypress(function(e) {

        var charcode = !e.charCode ? e.which : e.charCode;
        if (!(charcode == 46 || charcode >= 48 && charcode <= 57)) {
            e.preventDefault();
        }
    });

    $(".inputs12, .inputs7").keypress(function(e) {

        var charcode = !e.charCode ? e.which : e.charCode;
        if (!(charcode == 46 || charcode == 44 || charcode >= 48 && charcode <= 57)) {
            e.preventDefault();
        }
    });
    var randomizer = function() {
        var rnd = Math.round(Math.random() * 100);

        if (rnd < 40) {
            game.$apply(function() {
                if (game.hvostSkameikaAnim2 === 1) {
                    game.hvostSkameikaAnim2 = 2;
                } else {
                    game.hvostSkameikaAnim2 = 1;
                }
            });
        }

        game.$apply(function() {
            if (rnd < 20) {
                game.glazaSaving = true;
            } else {
                game.glazaSaving = false;
            }
        });

        if (game.talkingAnim) {
            game.$apply(function() {
                if (rnd < 33) {
                    game.rotSaving = true;
                } else {
                    game.rotSaving = false;
                }
            });
        }

        setTimeout(randomizer, 150);
    };

    setTimeout(randomizer, 100);

    var blink = function() {
        if (Math.round(Math.random() * 100) < 20) {
            $("#glaza").show();
        } else {
            $("#glaza").hide();
        }
        if (game.talkingAnim) {
            setTimeout(blink, 200);
        } else {
            $("#glaza").hide();
        }
    };
    $("#glaza").hide();
    setTimeout(blink, 200);

    var currentFrame = 1;
    var rotChange = function() {
        $("#rot").removeClass("rot_frame" + currentFrame);
        currentFrame = Math.round(Math.random() * 4);
        if (currentFrame != 3) {
            $("#rot").addClass("rot_frame" + currentFrame);
        }
        if (game.talkingAnim) {

        } else {
            $("#rot").removeClass("rot_frame" + currentFrame);
        }
        setTimeout(rotChange, 125);
    };

    setTimeout(rotChange, 500);
})